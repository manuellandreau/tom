# TOM API

[Nest](https://github.com/nestjs/nest) framwork

Choix: Nest est un framework COMPLET et éprouvé. Basé de ExpressJS (micro-framework) et l'architecture en module de Angular 2+, il permet une grande productivité.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Interactive Documentation

API doc (Swagger)
```
$ npm run start:dev
```
Then http://localhost:8000/api-doc

Compodoc : 
```
$ npm run compodoc
```
Then http://localhost:8080

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
