# TOM Client

[VueJS (CLI)](https://cli.vuejs.org) Framework

Choix: Vue 2 est un framework éprouvé chez gide et fourni une grande productivité. Vue 3.0.0 étant pour l'instant trop instable et en grand manque de compatibité avec l'écosysteme de la version 2 (01/01/10).

## Client

- [Tag system](http://www.vue-tags-input.com)
- [Vue-i18n](https://kazupon.github.io)
- [FilterBuilder](https://github.com/akumatus/FilterBuilder)
- [undraw illustrations](https://undraw.co)
- [Font Awesome icons](https://fontawesome.com)
- [Chart.js](https://vue-chartjs.org)
### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Traductions

Les fichier des traductions se trouvent dans **client/translations**.

### Compiles and minifies for production
```
npm run build
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Notes

Version = Nb de scénarios total possible = Nb de répondants  
Task = Nb de planches vues par répondant = Nb d'écrans  
Concept = Nb de produits présentés par planche/écran  
AttX = Variable X   
