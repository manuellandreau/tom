### Specs

L'idée est de demander à l'utilisateur de décrire son trade-off (variables, modalités, nombre de produits, nombre de répondants, etc...) puis de lui permettre de générer son plan d'expérience, calculer les utilités et importances et jouer avec un simulateur

https://app.diagrams.net/#G13eco9VuIt9-vpsXqbeIABLg4y67fIgJi

**État de l'art et concurrence :**
 - https://www.trade-off.fr/ (solution Solirem)
 - https://sawtoothsoftware.com/
 - https://conjointly.com/
 - https://www.qualtrics.com/fr/
