# TOM

- [Specifications](./documentation/specifications.md)
- [Documentation technique client](./documentation/technique-spa.md)
- [Documentation technique server](./documentation/technique-api.md)
- [Documentation d'utilisation](./documentation/utilisation.md)

**État de l'art et concurrence :**
 - https://www.trade-off.fr/ (solution Solirem)
 - https://sawtoothsoftware.com/

