# Heroku deployment script

cd client || exit
npm run build:prod
cd ../server || exit
heroku container:push web -a tom-gide
heroku container:release web -a tom-gide

echo 'Success ! '
echo 'TOM-GIDE is available on https://tom-gide.herokuapp.com'
