import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect(process.env.MONGO || 'mongodb+srv://tomgide:17Lanoue@tom-gide.lohzw.azure.mongodb.net/planxp?retryWrites=true&w=majority'),
  },
];
