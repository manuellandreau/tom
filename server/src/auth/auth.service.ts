import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const userFromDb = await this.usersService.findByEmailWithPassword(email);

    if (!userFromDb)
      throw new HttpException('Login user not found', HttpStatus.NOT_FOUND);

    const isValidPass = await bcrypt.compare(password, userFromDb.password);

    if (isValidPass) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = userFromDb;
      return result;
    } else {
      throw new HttpException('LOGIN.ERROR', HttpStatus.UNAUTHORIZED);
    }
  }

  async login({ _doc }: any) {
    const payload = { email: _doc.email, sub: _doc._id };
    return {
      access_token: this.jwtService.sign(payload, {
        secret: process.env.SECRET || '123456azerty',
      }),
    };
  }
}
