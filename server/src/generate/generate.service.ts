import {Injectable} from '@nestjs/common';
import { v4 as uuid } from 'uuid'
import {ITradoff} from "../schemas/user.schema";

@Injectable()
export class GenerateService {
    /**
     *
     * @param tradOffData
     */
    generate(tradOffData: ITradoff): string {
        const INIFile = this.createINI();
        const PARFile = this.createPAR(tradOffData);
        // const PHBFile = this.createPHB(tradOffData.contraints);

        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const execSync = require('child_process').execSync;
        const csvContent = execSync(`gawk -f $PWD/awk/Par2Des.awk FS="=" $PWD/awk/tmp/${INIFile} $PWD/awk/tmp/${PARFile}`); // $PWD/awk/tmp/${PHBFile}

        // Cache clean
        execSync(`rm $PWD/awk/tmp/${INIFile} $PWD/awk/tmp/${PARFile}`);

        return csvContent;
    }

    /**
     * TOdesign.INI, Program parameters for Par2Des.awk
     * @param INIOption
     */
    createINI(INIOption = null) {
        const tmpName = uuid();

        const content = `
            #--- Taille minimum stock d un attribut
            stock_sizmin = 1
            #--- Seuils pour nbessai (PTT GRD MAX)
            stock_nbessai = 10 20 100
            
            #--- Reglage q-equilibre: produits vs. attributs
            count_ratio = 1.10
            
            #--- Random generator
            seed = 7
        `;

        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const fs = require('fs');
        fs.writeFileSync(`awk/tmp/${tmpName}.ini`, content, 'utf8');
        return `${tmpName}.ini`;
    }

    /**
     * nJ                Nb Attributs/Descripteurs
     * labJ[j=1...nJ]    Code/Label Attributs
     * nK[j=1...nJ]      Nb Modalites par Attribut
     * nI                Nb Scenarios
     * nTbyI             Nb Taches(Planches) par Scenario
     * nPbyTI            Nb Produits(Concepts) par Tache*Scenario
     * @param tradOffData
     */
    createPAR(tradOffData: ITradoff) {
        const tmpName = uuid();

        const content = `
            nJ = ${tradOffData.variables.length}
            labJ = ${tradOffData.variables.map((_, i) => 'X' + (i + 1)).join(' ')}
            nK = ${tradOffData.variables.map(
                        v => v.type === 2
                                ? v.tags.length
                                : Math.floor((parseFloat(v.borneSup) - parseFloat(v.borneInf)) / parseFloat(v.step))
                    ).join(' ')}
            nI = ${tradOffData.sample.version}
            nTbyI = ${tradOffData.sample.task}
            nPbyTI = ${tradOffData.sample.concept}
        `;

        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const fs = require('fs');

        fs.writeFileSync(`awk/tmp/${tmpName}.par`, content, 'utf8');

        return `${tmpName}.par`;
    }

    /**
     * nPATPHB        Nombre de patterns prohibes
     * <pattern1>     Pattern (regular exp.) #1
     * <pattern2>     Pattern (regular exp.) #2
     * ...            ...
     * @param contraints
     */
    createPHB(contraints: any) {
        const tmpName = uuid();

        const content = `
            nPATPHB = 3
            A[2-3].*D2
            A1.*D[1-2]
            A2.*D1
        `;

        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const fs = require('fs');
        fs.writeFileSync(`awk/tmp/${tmpName}.phb`, content, 'utf8');
        return `${tmpName}.phb`;
    }
}
