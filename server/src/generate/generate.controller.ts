import {Controller, Post, Body, Res} from '@nestjs/common';
import * as csvToJson  from 'convert-csv-to-json';
import * as json2xls from 'json2xls'
import {GenerateService} from "./generate.service";

@Controller('generate')
export class GenerateController {
    constructor(private generateService: GenerateService) {}

    @Post('/csv')
    async generateCSV(@Res() res, @Body() tradOffData: any) {
        const data = this.generateService.generate(tradOffData);
        res.send(data);
    }

    @Post('/xls')
    async generateXML(@Res() res, @Body() tradOffData: any) {
        const data = csvToJson.csvStringToJson(this.generateService.generate(tradOffData).toString());
        res.end(json2xls(data), 'utf-8');
    }
}
