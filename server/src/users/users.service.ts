import { Model } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { IUser } from '../schemas/user.schema';
import { CreateUserDto } from './create-user.dto';
import {ITradoff} from '../schemas/user.schema';
import {log} from "util";

const saltRounds = 10;

@Injectable()
export class UsersService {
  constructor(
    @Inject('USER_MODEL')
    private userModel: Model<IUser>
  ) {}

  async create(newUser: CreateUserDto): Promise<IUser> {
    if (this.isValidEmail(newUser.email) && newUser.password) {
      const userRegistered = await this.findByEmail(newUser.email);
      if (!userRegistered) {
        newUser.password = await bcrypt.hash(newUser.password, saltRounds);
        const createdUser = new this.userModel(newUser);
        return await createdUser.save();
      } else {
        throw new HttpException(
          'Registration user already registered',
          HttpStatus.FORBIDDEN,
        );
      }
    } else {
      throw new HttpException(
        'Registration missing mandatory parameters',
        HttpStatus.FORBIDDEN,
      );
    }
  }

  isValidEmail(email: string) {
    if (email) {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    } else return false;
  }

  // async setPassword(email: string, newPassword: string): Promise<boolean> {
  //   const userFromDb = await this.findByEmail(email);
  //   if(!userFromDb) throw new HttpException('Login user not found', HttpStatus.NOT_FOUND);
  //
  //   userFromDb.password = await bcrypt.hash(newPassword, saltRounds);
  //
  //   await userFromDb.save();
  //   return true;
  // }

  async findAll(): Promise<IUser[]> {
    return this.userModel.find().exec();
  }

  async findByEmail(email: string): Promise<IUser | undefined> {
    return this.userModel.findOne({ email }, { password: 0, __v: 0 }).exec();
  }

  async findByEmailWithPassword(email: string): Promise<IUser | undefined> {
    return this.userModel.findOne({ email }).exec();
  }

  async saveTradoff(tradOffData: ITradoff, _id: string) {
    const dbUser = await this.userModel.findOne({ _id }).exec();
    if (dbUser.tradoffs.length > 9) dbUser.tradoffs.splice(0, 1)
    dbUser.tradoffs.push(tradOffData);
    return await dbUser.save();
  }
}
