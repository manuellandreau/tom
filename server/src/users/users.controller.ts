import {Body, Controller, Get, Post, Request, Res, UseGuards} from '@nestjs/common';
import { UsersService } from './users.service';
import {ITradoff} from "../schemas/user.schema";
import {AuthGuard} from "@nestjs/passport";

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getUsers(@Request() req) {
    return this.userService.findByEmail(req.user.email);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/tradoffs')
  async saveTradoff(@Request() req, @Res() res, @Body() tradOffData: ITradoff) {
    await this.userService.saveTradoff(tradOffData, req.user._id);
    res.send(`Tradoff ${tradOffData.name} saved.`);
  }
}
