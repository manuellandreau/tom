import { Document, Schema } from 'mongoose';

export const UserSchema = new Schema({
  username: String,
  email: String,
  password: String,
  tradoffs: Array
});

export interface IUser extends Document {
  readonly username: string;
  readonly email: string;
  readonly password: string;
  readonly tradoffs: ITradoff[]
}

export interface ITradoff extends Document {
  readonly name: string | null,
  readonly created: Date | null,
  readonly variables: any[],
  readonly contraints: any | null,
  readonly sample: any,
}
