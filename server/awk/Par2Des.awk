# TRADE-OFF - Program Par2Des.awk (Design Building)
# Version 2.01, 10 May 2016
# J.-M. Bernard, GIDE

# SYNTAX
# $ awk -f Par2Des.awk  [verbose] file.PAR file.INI >file.DES
#   "verbose": must be first argument
#   Filenames: [.PAR|.par] et [.INI|.ini] (for compatibility WIN/Linux]

# FUTUR / REVOIR
# * erreurs numeros & messages (20Mar14)
# - PRDCOUNT_RATIO: logique & implementation
# - REVOIR: principe/logique du test avec PRDCOUNT_RATIO
# - REVOIR: test evolutif avec ratio variable


BEGIN {

    #===== CONSTANTES SYMBOLIQUES (FIXES)
    symbcst();
    #--- Version du Programme
    PGM_NAME    = "Par2Des.awk";
    PGM_VERSION = "2.01";
    PGM_DATE    = "10 May 2016";
    #--- Valeurs pour prdtyp, pour nPRD*
    PRDTYP_VALID   = 1;
    PRDTYP_INVALID = 0;
    nPRD_UNKNOWN   = -1;
    #--- Valeurs pour prdreject
    PRDREJECT_PROHIBE = 1;
    PRDREJECT_INGROUP = 2;
    PRDREJECT_NOTRARE = 3;
    #--- Flags pour mode DEBUG (JMB only, use "FALSE" for final version)
DEBUG = TRUE; #TEMP
DEBUG_STOCK = TRUE; #TEMP
    DEBUG          = FALSE;
    DEBUG_STOCK    = FALSE;

    #===== PARAMETRES PROGRAM: DEFAULT VALUES
    MODE_VERBOSE = FALSE;
    prmini_defaults();


    #===== INITIALISATIONS
    #--- Entete fichier si verbose
    output_entete_done = FALSE;
    #--- Pour Comptage Patterns/Produits prohibes/valides
    nPRD     = 0;
    nPATPHB  = 0;
    nPRDVAL  = 0;
    nPRDPHB  = 0;
    #--- Pour Contrainte (EQ-3)
    nPRDUSED = 0;


    #===== LECTURE COMMAND LINE (FILES & OPTIONS)
    for(c=1 ; c<ARGC ; c++) {
        filnam = ARGV[c];    # qd argument = nom de fichier
        optnam = ARGV[c];    # qd argument = option
        if(filnam ~ FILINI_PTN) {
            filini = filnam;
            if(MODE_VERBOSE)
                printf("Command line: .INI File '%s' (input)\n", filini);
        }
        else if(filnam ~ FILPAR_PTN) {
            filpar = filnam;
            if(MODE_VERBOSE)
                printf("Command line: .PAR File '%s' (input)\n", filpar);
        }
        else if(filnam ~ FILPHB_PTN) {
            filphb = filnam;
            if(MODE_VERBOSE)
                printf("Command line: .PHB File '%s' (input)\n", filphb);
        }
        else if(optnam ~ "^FS=") {
        }
        else if(optnam ~ "^verbose") {
            MODE_VERBOSE = TRUE;
            ARGV[c]="";
            #--- Output: Entete
            output_entete();
        }
        else {
            error(600,ARGV[c]);
        }
    }
    if(MODE_VERBOSE) printf("\n");
}

{

    #===== READING FILES .INI & .PAR
    infile = TRUE;
    #--- Reading file .INI
    if     (FILENAME == filini) filini_read();
    #--- Reading file .PAR
    else if(FILENAME == filpar) filpar_read();
    #--- Reading file .PHB
    else if(FILENAME == filphb) filphb_read();
    #--- What else ?
    else error(601,FILENAME);

}

END {
    infile = FALSE;
  
    #===== INITIALISATIONS PRELIMINAIRES
    #--- Pour prd2dsc()
#    prd2dsc_init();

    #--- Initialiser random generator
    rand_init();

if(DEBUG) printf("01Z\n"); #TEMP
    #===== PRODUITS: CALCUL NB PRODUITS APRIORI
    #--- Nb Produits a priori: nPRD
    nPRD = 1;
    for(j=1 ; j<=nJ ; j++)
        nPRD *= nK[j];
if(DEBUG) printf("02Z\n"); #TEMP

    #===== DEF. DE nPRD_NOTTOOBIG, cf. prdtyp et prdlab
    #--- Is number of PRD's too big ?
    nPRD_NOTTOOBIG = TRUE;
    if(nPRD >= PRDMANY)
        nPRD_NOTTOOBIG = FALSE;
    #--- If not, enumerate & store in prdexist[]
    if(nPRD_NOTTOOBIG) {
        #--- First prd
        if(DEBUG) nPRDenum = 0;
        prd = prdenum_first();

        do {
            #--- Store prd in prdexist[]
            prdexist[prd] = TRUE;
            if(DEBUG) nPRDenum++;
            #--- Next prd
            prd = prdenum_next();
        } while(prd != PRDENUM_END);

        if(DEBUG) printf("nPRD=%d, nPRDenum=%d\n", nPRD,nPRDenum);
    }
    else {
        if(DEBUG) printf("nPRD is too big for enumeration: nPRD > %d !\n",PRDMANY);
    }


    #===== PRODUITS: DISTINCTION VALIDES/PROHIBES
    #--- Make prdlab[] (used for pattern matching), IF SPACE
    prdlab_init(nPRD);
    #--- Recherche produits prohibes/valides,       IF SPACE
    prdtyp_init(nPRD);
    #--- Nb produits valides/prohibes: nPRDVAL,nPRDPHB
    if(nPATPHB == 0)
        nPRDVAL = nPRD;
    else if(nPRD_NOTTOOBIG){
        nPRDVAL = nPRD;
        for(prd in prdexist) {
            if(prdtyp_get(prd) == PRDTYP_INVALID)
                nPRDVAL--;
            if(nPRDVAL==0) error(602,"");
        }
        nPRDPHB = nPRD-nPRDVAL;
    }
    else  {
        nPRDVAL = nPRD_UNKNOWN;
        nPRDPHB = nPRD_UNKNOWN;
    }

if(DEBUG) printf("04Z\n"); #TEMP


    #===== INITIALISATIONS
    #--- Init stock d'attributs
    prdstock_init();
    #--- Init pour prdstock_nbessai*()
    prdstock_nbessai_init();


    #===== OUTPUT
    #--- Output: Divers si VERBOSE
    if(MODE_VERBOSE) {
        #--- Output: Parametres du programme
        printf("\n");
        param_ini_output();
        #--- Output: Parametres du probleme
        printf("\n");
        param_par_output();
        #--- Output: Parametres du probleme/prohibitions
        printf("\n");
        param_phb_output();
    }

    #--- Output: Titre si VERBOSE
    if(MODE_VERBOSE) printf("Design\n\n");

    #--- Output: Columns Names
    printf("%s;%s;%s;%s",Ilab,Tlab,Plab,PRDlab);
    for(j=1 ; j<=nJ ; j++)
        printf(";%s",labJ[j]);
    printf("\n");


    #===== BOUCLE PRINCIPALE sur I*T
    for(i=1 ; i<= nI ; i++) {

        #----- Boucle sur les nTbyI taches
        for(t=1 ; t<= nTbyI ; t++) {

            #--- Choisir nPbyT produits parmi les nPRD
            for(p=1 ; p<=nPbyT ; p++) {
                if(DEBUG_STOCK) prdstock_printstatus();

                #--- Initialisation nb essais necessaires
                nbessai = prdstock_nbessai_set(0);
                if(DEBUG_STOCK) prdstock_check("01");
                #--- Obtenir 1 produit valide & nouveau ds le groupe
                do {

                    #--- Obtention 1 produit a partir du stock
                    prd = prdstock_get();
                    nbessai = prdstock_nbessai_inc();
                    prdtested[nbessai] = prd;
                    if(DEBUG_STOCK) prdstock_check("02");
                    #--- Contraintes sur prd

                    #--- prd Prohibe/Valide ?
                    if(prdtyp_isvalid(prd)==FALSE) {
                        prdstock_reject(prd,PRDREJECT_PROHIBE);
                        continue;
                    }

                    if(DEBUG_STOCK) prdstock_check("03");
                    #--- prd New/Old (dans le groupe = la tache) ?
                    prd_isnew = TRUE;
                    if(p>1) for(p1=1 ; p1<p ; p1++) {
                        if(prd == prdgrp[p1])
                            prd_isnew = FALSE;
                    }
                    if(prd_isnew == FALSE) {
                        prdstock_reject(prd,PRDREJECT_INGROUP);
                        continue;
                    }
                    
                    if(DEBUG_STOCK) prdstock_check("04");
                    #--- prd deja Utilise: Sur-represente (Reject) ou Rare (Accept) ?
                    if(prd in prd_count) {
                        prd_count_moy = (nPRDUSED+1)/nPRDVAL + 0;
                        if(prd_count[prd]/prd_count_moy >= PRDCOUNT_RATIO) {
if(DEBUG) {
  printf("TooFrequent prd=%s nPRDVAL=%d nPRDUSED=%d  countobs=%d countmoy=%.2lf\n",
    prd,nPRDVAL,nPRDUSED,prd_count[prd],prd_count_moy);
}
                            prdstock_reject(prd,PRDREJECT_NOTRARE);
                            continue;
                        }
                    }
                    #--- prd non Utilise, donc Accept
                    else ;

                    if(DEBUG_STOCK) prdstock_check("05");
                    #--- Produit prd n'est rejete par aucun test !
                    break;

                } while (1);

                if(DEBUG_STOCK) prdstock_check("06");
                #===== Produit prd est accepte
                #--- Le mettre dans le groupe/tache en cours
                prdgrp[p] = prd;

                #--- Le retirer du stock
                if(DEBUG_STOCK) prdstock_check("07");
                prdstock_accept(prd);
                if(DEBUG_STOCK) prdstock_check("08");
                nPRDUSED ++;

                #--- Sortie du design (pour i,t,p courants)
                printf("%d;%d;%d;%s", i,t,p,prd);
                prd2dsc(prd); # sets dsccur[]
                for(j=1 ; j<=nJ ; j++)
                    printf(";%d", dsccur[j]);
                printf("\n");

                #--- See prd's rejected (if VERBOSE mode)                   
                prdstock_flushessais(1);

                #--- Actualiser prd_count
                if(prd in prd_count) 
                    prd_count[prd] ++;
                else
                    prd_count[prd] = 1;
            }
        }

        #----- DEBUG: Stock status
        if(DEBUG_STOCK) prdstock_printstatus();

    }


    #===== NORMAL END
    if(MODE_VERBOSE) {
        printf("\nPROGRAM TERMINATED NORMALY\n\n");

        #--- See Stock status
        prdstock_printstatus();

        #--- See rejection stats
        prdstock_printrejected();

        #--- See Counts of prd's
        if(DEBUG) {
            output_prdcounts();
        }
    }


    #===== RUBBISH
    unused(filini_isread); unused(filpar_isread);
}

# Function used only in VERBOSE mode
function output_prdcounts()
{
    printf("Counts of valid 'prd' for all I\n");

    if(nPRD_NOTTOOBIG) {
        for(prd in prdexist) {
            #--- Get attributs dsc[] du produit prd et validite
            prd2dsc(prd); # sets dsccur[]
            prdtyp = prdtyp_get(prd);
            if(prdtyp == PRDTYP_INVALID) continue;
            #--- Impression prd,dsc[],label,validity
            printf("prd=%s ", prd);
            printf(" dsc=");
            for(j=1 ; j<=nJ ; j++)
                printf("%d ", dsccur[j]);
            printf(" valid=%d ", prdtyp);
            printf(" count=%5d %.2lf", prd_count[prd],prd_count_moy);
            printf("\n");
        }
        printf("\n");
    }
    else {
       for(prd in prd_count) {
            #--- Get attributs dsc[] du produit prd
            prd2dsc(prd); # sets dsccur[]
            prdtyp = PRDTYP_VALID;
            #--- Impression prd,dsc[],label,validity
            printf("prd=%s ", prd);
            printf(" dsc=");
            for(j=1 ; j<=nJ ; j++)
                printf("%d ", dsccur[j]);
            printf(" valid=%d ", prdtyp);
            printf(" count=%d", prd_count[prd]);
            printf("\n");
        }
        printf("\n");
    }

}


#===============================
# CONSTANTES SYMBOLIQUES (FIXES)
# 10 May 2016
#===============================

function symbcst()
{
    #===== SYSTEME
    TO_SYSTEM  = "Linux";
    #--- Valeur pour PRDMANY selon system
    if(TO_SYSTEM == "Linux") PRDMANY = 50000;
    else                     PRDMANY =  1000;

    #===== Logical constants
    TRUE = 1;
    FALSE = 0;

    #===== FILENAME patterns & initialisation
    FILINI_PTN = ".*\\.[Ii][Nn][Ii]";
    FILPAR_PTN = ".*\\.[Pp][Aa][Rr]";
    FILDES_PTN = ".*\\.[Dd][Ee][Ss]";
    FILLGC_PTN = ".*\\.[Ll][Gg][Cc]";
    FILPHB_PTN = ".*\\.[Pp][Hh][Bb]";
    filini = "";
    filpar = ""; 
    fildes = ""; 
    fillgc = "";
    filphb = "";
    filini_isread = FALSE;
    filpar_isread = FALSE;
    fildes_isread = FALSE;
    fillgc_isread = FALSE;
    filphb_isread = FALSE;

    FILERR = "/dev/stderr";

    #===== LABELS COLONNES (dans .CSV)
    Ilab   = "IDScn";
    Tlab   = "Task";
    Plab   = "ProdNum";
    PRDlab = "Prod";
    VALlab = "Valid";

    #===== VARIOUS
    #--- Flag indicating that enumeration of prd's is finished
    PRDENUM_END = "-1";
}
#================================================
# CONVERSION prd <-> dsc[] & ENUMERATION OF prd's
# 10 May 2016
#================================================

# GLOBALS: 
#   dsccur[], prd2dsc_isinit, prd2dsc_div[]
# FUNCTIONS
#   dsc2prd(dsc, )
#   prd2dsc(prd, )
#   prdenum_first()
#   prdenum_next()

# NOTES
# - These are new versions of functions prd2dsc() & dsc2prd():
#   for dealing with BIG TO, prd is now a string, not an int/float


#=========================
# CONVERSION prd <-> dsc[]
#=========================

function dsc2prd(dsc, j,prdstr,labj)
{
    PRD2DSC_SEP = "-";
    prdstr = sprintf("%d",dsc[1]);
    for(j=2 ; j<=nJ ; j++) {
        labj = sprintf("%s%d",PRD2DSC_SEP,dsc[j]);
        prdstr = prdstr labj;
    }
    return prdstr;
}

function prd2dsc(prdstr, j,nJ2)
{
#FUTUR: si pb de vitesse, dsc -> dsccur directement ?
    PRD2DSC_SEP = "-";
    nJ2 = split(prdstr,dsc,PRD2DSC_SEP);
    if(nJ2 != nJ) {
#REVOIR: message erreur avec parametres, non en mode DEBUG
        if(DEBUG) printf("Pb with prd='%s' nJ2=%d nJ=%d\n", prdstr,nJ2,nJ);
        error(200);
    }

    for(j=1 ; j<=nJ ; j++)
        dsccur[j] = dsc[j];

    #--- "Return" dsccur[]
}


#===============================
# ENUMERATION OF prd's
# Loop over prd's is done by:
#
#   prd = prdenum_first();
#   do {
#     ...
#     prd = prdenum_next();
#   } while(prd != PRDENUM_END);
#
#===============================

function prdenum_first(  j,prd)
{
    for(j=1 ; j<=nJ ; j++) 
        dsccur[j] = 1;
    prd = dsc2prd(dsccur);
    return prd;
}

function prdenum_next(  j,j1,prd)
{
    if(dsccur[nJ] < nK[nJ]) {
        dsccur[nJ]++;
        prd = dsc2prd(dsccur);
        return prd;
    }
    else {
        for(j=nJ; j>=1 ; j--) {
            if(dsccur[j] != nK[j]) 
                break;
        }
        if(j>=1) {
            dsccur[j]++;
            for(j1=j+1 ; j1<=nJ ; j1++)
                dsccur[j1]=1;
            prd = dsc2prd(dsccur);
            return prd;
        }
        else return PRDENUM_END;
    }
}        


#=====================================
# OLD VERSIONS (NOT USED ANYMORE)
#   prd was considered as an int/float
#=====================================

function dsc2prd_v1(dsc, j,k,coef,prd)
{
    prd = 1;
    coef = 1;
    for(j=nJ ; j>=1 ; j--) {
        k = dsc[j];
        prd += (k-1)*coef;
        coef *= nK[j];
    }
    return prd;
}

function prd2dsc_init_v1()
{
    prd2dsc_isinit = FALSE;
}

function prd2dsc_v1(prd, j,reste)
{

    #--- Initialisation des diviseurs successifs prd2dsc_div[]
    if(prd2dsc_isinit_v1 == FALSE) {
        prd2dsc_div[nJ] = 1;
        for(j=nJ-1 ; j>=1 ; j--) {
            prd2dsc_div[j] = prd2dsc_div[j+1] * (nK[j+1]+0);
        }
        prd2dsc_isinit = TRUE;
    }

    #--- Formule avec divisions successives
    reste = prd-1;
    for(j=1 ; j<=nJ ; j++) {
        dsccur[j] = int(reste / prd2dsc_div[j]) + 1;
        reste = reste % prd2dsc_div[j];
    }

    #--- "Return" dsccur[]
}

#=====================================
# GESTION PRODUITS VALIDES/NON-VALIDES
# 10 May 2016
#=====================================

# GLOBAL: 
#   prdtyp_valid[prd], prd in prdexist
#   prdexist[prd]
# FUNCTIONS
#   prdtyp_init()
#   prdtyp_set()
#   prdtyp_get()
#   prdtyp_isvalid()
#   prdtyp_setall()

function prdtyp_init(nPRD)
{
    #--- SET'S prdtyp_valid[] for all prd's
    if(nPRD_NOTTOOBIG == TRUE)
        prdtyp_setall();

    return;
}

function prdtyp_set(prd,prdtyp)
{
    prdtyp_valid[prd] = prdtyp;
}

function prdtyp_get(prd)
{
    if(nPATPHB == 0) return PRDTYP_VALID;

    if(nPRD_NOTTOOBIG == TRUE)
        return(prdtyp_valid[prd]);

    else {
        #--- Initialisation
        prdtyp = PRDTYP_VALID;
        prdlab = prdlab_get(prd);

        #--- Boucle sur patterns prohibes
        for(pat=1 ; pat<=nPATPHB ; pat++) {
            pattern = patphblab[pat];
            if(prdlab ~ pattern) {
                prdtyp = PRDTYP_INVALID;
                return prdtyp;
            }
        }
 
        return prdtyp;
    }
}

function prdtyp_isvalid(prd)
{
    if(prdtyp_get(prd) == PRDTYP_VALID)
        return TRUE;
    return FALSE;
}

function prdtyp_setall()
{
    #===== TEST TAILLE nPRD
    if(nPRD_NOTTOOBIG == FALSE) 
        return;

    #===== CONSTRUCTION DES produits valides/prohibes=invalides
    #--- Initialisation
    for(prd in prdexist)
        prdtyp_set(prd,PRDTYP_VALID);

    #--- Boucle sur patterns invalides & marquage des produits prd qui matchent
    for(pat=1 ; pat<=nPATPHB ; pat++) {
        pattern = patphblab[pat];
        nPRDREJECTED = 0;
        for(prd in prdexist) {
            if(prdtyp_get(prd) == PRDTYP_VALID) {    # if not invalidated before
                if(prdlab_get(prd) ~ pattern) {      # & if matches prohib. pattern
                    prdtyp_set(prd,PRDTYP_INVALID);
                    nPRDREJECTED ++;
                }
            }
        }
        patphbeff[pat] = nPRDREJECTED;
        #--- Le pattern ne rejete aucun prd (redondant, mal specifie ?)
        if(nPRDREJECTED==0)
            warning(320,pattern);
    }
}


#=================
# PRDLAB functions
# 10 May 2016
#=================


function prdlab_init(nPRD)
{
    # REVOIR: fonction desactivee !
    return;
}

function prdlab_get(prd,  lab,j,k,sk)
{
    #--- Get attributes du produit prd: sets dsccur[]
    prd2dsc(prd);

    #--- Build label of prd from dsccur[]
    lab = "";
    for(j=1 ; j<=nJ ; j++) {
        k = dsccur[j];
        sk = sprintf("%d",k);
        lab = lab labJ[j] sk;
    }

    #--- Return label
    return(lab);
}



#==================================
# INPUT .PAR: PARAMETERS OF PROBLEM
# 10 May 2016
#==================================

# REVOIR
# * nPbyT vs nPbyTI: both accepted, nPbyT in doc only
# - tests syntaxe par=parval correcte

function filpar_read()
{

    #--- Things to do once
    if(FNR == 1) {
        filpar_isread = 1;
        #--- Mode VERBOSE
        if(MODE_VERBOSE)
            printf("Reading .PAR file '%s' FS='%s'\n",FILENAME,FS);
    }

    #--- Read parameters: nJ, labJ[], nK[], nI, nTbyI, nPbyT
    param_read();
}

function param_read()
{
    #----- INPUT: PARAMETRES DU PROBLEME

    #--- Skip comments + blank lines
    if($0 ~ "^#") next;
    if(length($0) == 0) next;
    if(NF == 0) next;

    #--- Split line into: parnam = parval[]
    if(NF != 2) {
        warning(110,$0);
        next;
    }
    else {
        n1 = split($1,parnam," ");
        n2 = split($2,parval," ");
        if(n1 == 1) {
            if(MODE_VERBOSE) printf("  Reading param-pb '%s'\n", parnam[1]);
            $1 = parnam[1];
        }
        else {
            error(111,$1);
        }
    }

    #--- Nb Attributs, nJ
    if($1 == "nJ") {
        if(n2 == 1) 
            nJ = $2+0;
        else
            error(112,parval);
    }
    #--- Labels des attributs, labJ[1...nJ]
    else if($1 == "labJ") {
        if(n2 != nJ) error(113);
        for(j=1 ; j<=nJ ; j++)
            labJ[j] = parval[j];
    }
    #--- Nb modalites des attributs, nK[1...nJ]
    else if($1 == "nK") {
        if(n2 != nJ) error(114);
        for(j=1 ; j<=nJ ; j++)
            nK[j] = parval[j]+0;
    }
    #--- Nb Individus/Scenarios
    else if($1 == "nI") {
        nI = $2+0;
    }
    #--- Nb taches par Individu
    else if($1 == "nTbyI") {
        nTbyI = $2+0;
    }
    #--- Nb Produits par Tache
    else if($1 == "nPbyT" || $1 == "nPbyTI") {
        nPbyT = $2+0;
    }
    #--- Unknown parameter
    else {
        warning(115,$1);  
    }
}

function param_par_output()
{
    printf("Parameters-Problem (input from .PAR)\n");

    printf("  nI = %d\n", nI);
    printf("  nJ = %d\n", nJ);
    printf("  labJ = [");
    for(j=1 ; j<=nJ ; j++)
        printf("%s ",labJ[j]);
    printf("]\n");
    printf("  nK = [");
    for(j=1 ; j<=nJ ; j++)
        printf("%d ",nK[j]);
    printf("]\n");
    printf("  nTbyI = %d\n", nTbyI);
    printf("  nPbyT = %d\n", nPbyT);

    printf("Parameters-Problem (computed)\n");

    nPbyI = nPbyT*nTbyI;
    printf("  nPbyI = %d\n", nPbyI);
    printf("  nP = %.0lf (Nb products used in TO)\n", nPbyI*nI);
    printf("  nPRD = %.0lf (Nb products possible a priori)\n", nPRD);
    printf("\n");
}



#================================
# INPUT .PAR: PARAMETRES PROBLEME
# 10 May 2016
#================================

function filphb_read()
{
    #--- Things to do once
    if(FNR == 1) {
        filphb_isread = 1;
        nPATPHB = 0;
        #--- Mode VERBOSE
        if(MODE_VERBOSE)
            printf("Reading .PHB/.phb file '%s' FS='%s'\n",FILENAME,FS);
    }

    #--- Read parameters: nPATPHB + liste
    prohib_read();
}

function prohib_read()
{
    #----- INPUT: PATTERNS PROHIBES

    #--- Skip comments + blank lines
    if($0 ~ "^#") next;
    if(length($0) == 0) next;
    if(NF == 0) next;

    #--- Split line into: parnam = parval[]
    if(NF != 2) {
        warning(120,$0);
        next;
    }
    else {
        n1 = split($1,parnam," ");
        n2 = split($2,parval," ");
        if(n1 == 1) {
            if(MODE_VERBOSE) printf("  Reading param '%s'\n", parnam[1]);
            $1 = parnam[1];
        }
        else {
            error(121,$1);
        }
    }


    #--- Nb et Liste Patterns Prohibes
    if($1 == "nPATPHB") {
        nPATPHB = $2+0;
        oldFS = FS;
        for(pat=1 ; pat<=nPATPHB ; pat++) {
            FS=" ";
            do {
                getline;
                skipline = 0;
                if($0 ~ "^#")            skipline=1;
                else if(length($0) == 0) skipline=1;
                else if(NF == 0)         skipline=1;
            } while(skipline==1);
            patphblab[pat] = $1;
        }
        FS = oldFS;
    }
    else {
        warning(122,$1);  
    }
}

function param_phb_output()
{
    #--- PERCENTAGE of Prohibited products for warning
    WARNPHBRATIO = 0.20;

    printf("Parameters-Prohibitions (input from .PHB)\n");
    printf("  nPATPHB = %d\n", nPATPHB);
    for(pat=1 ; pat<=nPATPHB ; pat++)
        printf("  %s\n", patphblab[pat]);
    if(nPATPHB == 0)
        warning(129);
    printf("\n");

    printf("Nb Products valid/prohibited (computed from .PHB)\n");
    printf("  nPRD = %d\n", nPRD);
    printf("  nPRDVAL = %d (%d if Unknown)\n", nPRDVAL,nPRD_UNKNOWN);
    printf("  nPRDPHB = %d (%d if Unknown)\n", nPRDPHB,nPRD_UNKNOWN);
    if(nPRDPHB == 0)
        warning(128);
    printf("\n");

    if(nPATPHB == 0) {
        printf("No Products prohibited: NO prohibition\n");
    }
    else if(nPRD_NOTTOOBIG == TRUE) {
        printf("Nb Products prohibited by each prohibition (computed from .PHB)\n");
        for(pat=1 ; pat<=nPATPHB ; pat++) {
            printf("  %s -> %d\n", patphblab[pat],patphbeff[pat]);
            if(patphbeff[pat]/nPRD > WARNPHBRATIO)
                warning(127,patphblab[pat]);
        }
        for(pat=1 ; pat<=nPATPHB ; pat++) {
            if(patphbeff[pat] == 0)
                warning(123,patphblab[pat]);
        }
    }
    else {
        printf("Nb Products prohibited by each prohibition: NOT computed\n");
    }

    printf("\n");
}

#==================================
# PARAMETERS OF PROGRAM Par2Des.awk
# 10 May 2016
#==================================

# Globals
#  ??
# Functions
#   prmini_defaults()
#   filini_read()
#   prmini_read()
#   param_ini_output()

function prmini_defaults()
{
    #--- Parametres pour: Gestion stock, Default values
    PRDSTOCK_SIZMIN      =  1;
    PRDSTOCK_INCREMENT   =  1;
    PRDSTOCK_NBESSAI_PTT = 20;
    PRDSTOCK_NBESSAI_GRD = 50;
    PRDSTOCK_NBESSAI_MAX = 100;
    PRDCOUNT_RATIO       = 1.10;

    #--- Parametres pour: rand()
    SEED = 0;
    SEED_USE = FALSE;

    #--- Mode VERBOSE ?
    MODE_VERBOSE = FALSE;
}

function filini_read()
{
    #--- Things to do once
    if(FNR == 1) {
        filini_isread = 1;
        #--- Mode VERBOSE
        if(MODE_VERBOSE)
            printf("Reading .INI/.ini file '%s' FS='%s'\n",FILENAME,FS);
    }

    #--- Read program parameters
    prmini_read();
}

function prmini_read()
{
    #----- INPUT .INI: PARAMETRES DU PROGRAMME

    #--- Skip comments + blank lines
    if($0 ~ "^#") next;
    if(length($0) == 0) next;
    if(NF == 0) next;

    #--- Split line into: parnam = parval[]
    if(NF != 2) {
        warning(100, $0);
        next;
    }
    else {
        n1 = split($1,parnam," ");
        n2 = split($2,parval," ");
        if(n1 == 1) {
            if(MODE_VERBOSE) printf("  Reading param-pgm '%s'\n", parnam[1]);
            $1 = parnam[1];
        }
        else {
            error(101,$1);
        }
    }

    #--- Stock minimum size per attribute
    if($1 == "stock_sizmin") {
        PRDSTOCK_SIZMIN = $2+0;
    }
    else if($1 == "stock_increment") {
        PRDSTOCK_INCREMENT = $2+0;
    }
    else if($1 == "stock_nbessai") {
        for(c=1 ; c<=3 ; c++) {
            value = parval[c]+0;
            if(c==1) PRDSTOCK_NBESSAI_PTT = value;
            if(c==2) PRDSTOCK_NBESSAI_GRD = value;
            if(c==3) PRDSTOCK_NBESSAI_MAX = value;
        }
    }
    else if($1 == "count_ratio") {
        PRDCOUNT_RATIO = $2+0;
    }
    else if($1 == "seed") {
        value = $2+0;
        SEED_USE = TRUE;
        SEED = value;
    }
    else {
        warning(102,$1);
    }
}

function param_ini_output()
{
    printf("Parameters-Program (default, modified by cmdline + .INI)\n");

    printf("  stock_sizmin = %d\n", PRDSTOCK_SIZMIN);
    printf("  stock_increment = %d\n", PRDSTOCK_INCREMENT);
    printf("  stock_nbessai = %d %d %d\n",
        PRDSTOCK_NBESSAI_PTT,PRDSTOCK_NBESSAI_GRD,PRDSTOCK_NBESSAI_MAX);
    printf("  count_ratio = %.2lf\n", PRDCOUNT_RATIO);
    if(SEED_USE)
        printf("  seed_use=YES, seed=%.1lf (int part used)\n", SEED);
    else
        printf("  seed_use=NO\n");
    printf("  verbose = %d\n", MODE_VERBOSE);
}

#=============================
# GESTION DU STOCK D'ATTRIBUTS
# 10 May 2016
#=============================

# Fonctions:
#     prdstock_init()
#     prdstock_setrelax(level)
#     prdstock_incrementall()
#     prdstock_incrementone(j1)
#     prdstock_status()
# Variables gerees:
#     prdstock_siz[nJ]
#     prdstock_tab[nJ,nK[j]]
#     prdstock_nbessai
#     prdstock_nbtested
#     prdstock_rejected[why]

function prdstock_init()
{
    for(j=1 ; j<=nJ ; j++) {
        prdstock_siz[j] = 0;
        for(k=1 ; k<=nK[j] ; k++) {
            prdstock_tab[j,k] = PRDSTOCK_INCREMENT;
            prdstock_siz[j] += PRDSTOCK_INCREMENT;
        }
    }
}

function prdstock_setrelax(level)
{
    prdstock_relax = level;

    if(level == 0) return;
    else if(level == 1 || level == 2) {
        if(DEBUG_STOCK) {
            printf("Stock problem, level %d\n", prdstock_relax);
            prdstock_printstatus();
        }
        return;
    }
    else if(level == 3) {
        printf("\n");
        printf("FATAL-ERROR: nbessais too big=%d\n", prdstock_nbessai);

        printf(" PRD's rejected=");
        for(e=1 ; e<prdstock_nbessai ; e++)
            printf("%s,",prdtested[e]);
        printf("\n");

        prdstock_printstatus();
        output_prdcounts();
        error(400);
        exit(1);
    }
}

function prdstock_nbessai_init()
{
    prdstock_nbessai_set(0);
}

function prdstock_nbessai_set(nbessai)
{
    return prdstock_nbessai = nbessai;
}

function prdstock_nbessai_get()
{
    return prdstock_nbessai;
}

function prdstock_nbessai_inc()
{
    prdstock_nbessai++;
    return prdstock_nbessai;
}

function prdstock_flushessais(location)
{
    if(MODE_VERBOSE) {
        printf("   nbessais(%d)=%2d", location,prdstock_nbessai);
        if(nbessai > 1) {
            printf(" rejected=");
            for(e=1 ; e<prdstock_nbessai ; e++)
                printf("%s,",prdtested[e]);
        }
        printf("\n");
    }
}

# Ajoute PRDSTOCK_INCREMENT (=1 par defaut) exemplaire(s) de chaque 
# modalite k, pour chaque attribut j, a condition que l'attribut 
# ait une/des modalite(s) manquante(s)
function prdstock_incrementall( j,k)
{
    if(DEBUG_STOCK) {
        printf("Increment all, before\n");
        prdstock_printstatus();
    }

    for(j=1 ; j<=nJ ; j++) {
        missingmod = FALSE;
        for(k=1 ; k<=nK[j] ; k++) {
            if(prdstock_tab[j,k] == 0)
                missingmod = TRUE;
        }
        if(missingmod) {
            for(k=1 ; k<=nK[j] ; k++) {
                prdstock_tab[j,k] += PRDSTOCK_INCREMENT;
                prdstock_siz[j] += PRDSTOCK_INCREMENT;
            }
        }
    }

    if(DEBUG_STOCK) {
        printf("Increment all, after\n");
        prdstock_printstatus();
    }
}

# Ajoute PRDSTOCK_INCREMENT (=1 par defaut) exemplaire(s) de chaque 
# modalite k, pour l'attribut j1 uniquement, a condition que j1 ait 
# une/des modalite(s) manquante(s)
function prdstock_incrementone(j1, k)
{
    if(DEBUG_STOCK) {
        printf("Increment atb=%d, before\n", j1);
        prdstock_printstatus();
    }

    missingmod = FALSE;
    for(k=1 ; k<=nK[j1] ; k++) {
        if(prdstock_tab[j1,k] == 0)
            missingmod = TRUE;
    }
    if(missingmod) {
        for(k=1 ; k<=nK[j1] ; k++) {
            prdstock_tab[j1,k] += PRDSTOCK_INCREMENT;
            prdstock_siz[j1] += PRDSTOCK_INCREMENT;
        }
    }

    if(DEBUG_STOCK) {
        printf("Increment atb=%d, after\n", j1);
        prdstock_printstatus();
    }
}

# Affiche l'etat du stock
function prdstock_printstatus( j,k)
{
    printf("Stock Status\n");
    for(j=1 ; j<=nJ ; j++) {
        printf("  %-2s ", labJ[j]);
        for(k=1 ; k<=nK[j] ; k++) {
            printf("%d ", prdstock_tab[j,k]);
        }
        printf(" Total=%d\n", prdstock_siz[j]);
    }
    printf("\n");
}

# Accepte produit prd,  Stock est reduit 
function prdstock_accept(prd, j,k)
{
    if(DEBUG_STOCK) printf("Accept=%s\n",prd);

    prd2dsc(prd); # sets dsccur[]
    if(DEBUG_STOCK) {prdstock_show("Ready to accept",prd,dsccur);}
    for(j=1 ; j<=nJ ; j++) {
        k = dsccur[j];
        prdstock_tab[j,k] --;
        prdstock_siz[j] --;
    }

    #--- Store for Acc_Rej/Tested ratio
    prdstock_nbtested ++;
}

# Rejete produit prd, Stock reste inchange
function prdstock_reject(prd,why)
{
    if(DEBUG_STOCK) printf("Reject=%s (why=%d) ",prd,why);
    prdstock_rejected[why] ++;

    #--- Store for Acc_Rej/Tested ratio
    prdstock_nbtested ++;

    return;
}

# Affiche Nb produits acceptes/rejetes et pourquoi
function prdstock_printrejected( why,nbwhy)
{
    #--- Relevant figures
    prdstock_nbacc = nPbyI*nI;
    prdstock_ratacc = prdstock_nbacc / prdstock_nbtested;

    #--- Affichage Numbers + Ratio: tested + accepted
    printf("Number/proportion of products tested, accepted/rejected\n");
    printf("  Tested : %d\n", prdstock_nbtested);
    printf("    Accepted                  : %d (%8.6lf)\n", prdstock_nbacc,prdstock_ratacc);

    #--- Affichage Numbers + Ratio: rejected
    nbwhy = 0;
    for(why in prdstock_rejected) {
        nbwhy++;
        prdstock_nbrejwhy = prdstock_rejected[why];
        prdstock_ratrejwhy = prdstock_nbrejwhy / prdstock_nbtested;
        if(why == PRDREJECT_PROHIBE) {
            printf("    Rejected (prohibited)     : %d (%8.6lf)\n", 
                prdstock_nbrejwhy,prdstock_ratrejwhy);
        }
        else if(why == PRDREJECT_NOTRARE) {
            printf("    Rejected (too frequent)   : %d (%8.6lf)\n",
                prdstock_nbrejwhy,prdstock_ratrejwhy);
        }
        else if(why == PRDREJECT_INGROUP) {
            printf("    Rejected (already in task): %d (%8.6lf)\n",
                prdstock_nbrejwhy,prdstock_ratrejwhy);
        }
        else {
            printf("    Rejected (unknown)        : %d  (%8.6lf)\n",
                prdstock_nbrejwhy,prdstock_ratrejwhy);
        }
    }
    if(nbwhy == 0)
        printf("    0\n");

    printf("\n");
}


# Recherche un prd dans le stock
# Gestion souplesse, reassortiment, tirage au hasard
# FUTUR: dissocier 2 parties dans cette fonction
# (1) souplesse et reassortiment
# (2) tirage d'un produit
function prdstock_get( j,k)
{
    #--- Niveau de souplesse ds gestion stock selon nbessai deja tentes
    nbessai = prdstock_nbessai_get();

    if     (nbessai >= PRDSTOCK_NBESSAI_MAX) prdstock_setrelax(3);
    else if(nbessai >= PRDSTOCK_NBESSAI_GRD) prdstock_setrelax(2);
    else if(nbessai >= PRDSTOCK_NBESSAI_PTT) prdstock_setrelax(1);
    else                                     prdstock_setrelax(0);

    #--- Reassortiment stock selon niveau de prdstock_relax
    if(prdstock_relax == 0) {
        for(j=1 ; j<=nJ ; j++) {
            if(prdstock_siz[j] < PRDSTOCK_SIZMIN )
                prdstock_incrementone(j);
        }
    }
    else if(prdstock_relax == 1) {
        for(j=1 ; j<=nJ ; j++) {
            if(prdstock_siz[j] < PRDSTOCK_SIZMIN+2  ||  prdstock_isattcst(j) )
                prdstock_incrementone(j);
        }
    }
    else if(prdstock_relax == 2) {
        for(j=1 ; j<=nJ ; j++) {
            j_needs_reassort = 0;
            for(k=1 ; k<=nK[j] ; k++) {
                if(prdstock_tab[j,k] == 0) {
                    j_needs_reassort = 1; 
                    break;
                }
            }
            if(j_needs_reassort)
                prdstock_incrementone(j);
        }
#REVOIR si cette nouvelle version fonctionne mieux !
#        prdstock_flushessais(2);
#        prdstock_nbessai_set(0);
    }

    #--- Tirage au hasard dans stock
    for(j=1 ; j<=nJ ; j++) {
        do {
            k = randint(nK[j]);
        } while (prdstock_tab[j,k] == 0);
        prdstock_dsc[j] = k;
    }
    return dsc2prd(prdstock_dsc);
}

function prdstock_isattcst(j, k,nknotempty)
{
    nknotempty = 0;
    for(k=1 ; k<=nK[j] ; k++) {
        if(prdstock_tab[j,k] > 0)
            nknotempty ++;
        if(nknotempty >= 2) return FALSE;
    }
    return TRUE;
}


# IF DEBUG_STOCK ONLY
# Permet de traquer ds Par2Des.awk (boucle de construction 
# des produits, avec accept/reject) des problemes ds stock
#REVOIR: meilleur gestion de cette erreur / message
function prdstock_check(where)
{
    stock_problem = 0;

    for(j=1 ; j<=nJ ; j++) { 
        if(prdstock_siz[j] < 0) {
            stock_problem = 1;
            break;
        }           
        for(k=1 ; k<=nK[j] ; k++) {
            if(prdstock_tab[j,k] < 0) {
                stock_problem = 1;
                break;
            }
        }
        if(stock_problem) break;
    }

    if(stock_problem == 1) {
        printf("\nFATAL-ERROR (Internal) '%s'\n", where);
        exit(1);
    }
}

# IF DEBUG_STOCK ONLY
function prdstock_show(txt,prd,dsc)
{
    printf("\n%20.20s : prd = %s dsc = ", txt,prd);
    for(j=1 ; j<=nJ ; j++) {
        printf("%d ",dsc[j]);
    }
    printf("\n");
}
#===============
# RANDOM NUMBERS
# 10 May 2016
#===============

function rand_init()
{
    if(SEED_USE) srand(SEED);
    else         srand();
}

#----- FUNCTION randint(n), Returns random integer in [1,...n]
function randint(n)
{
    return (1 + int(rand() * n) );
}

#===============
# ENTETE
# 10 May 2016
#===============

function output_entete()
{
    if(output_entete_done == FALSE) {
        printf("TRADE-OFF - %s\n",PGM_NAME);
        printf("Version %s, %s, GIDE\n\n",PGM_VERSION,PGM_DATE);
        output_entete_done = TRUE;
    }
}
#================
# ERRORS
#================

# REVOIR
# - distinguer warning's de info's ?

# Globals
# Functions
#   error()
#   warning()
#   errnum2mess()
#   unused()

function error(errnum,errparam)
{
    errmess = errnum2mess(errnum);

    if(infile == TRUE) {
        if(errparam == "") {
            printf("%s, Error %d reading file '%s' at line %d; %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess) >FILERR;
        }
        else {
            printf("%s, Error %d reading file '%s' at line %d; %s: %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess,errparam) >FILERR;
        }
    }
    else {
        if(errparam == "") {
            printf("%s, Error %d, %s\n",
                PGM_NAME,errnum,errmess) >FILERR;
        }
        else {
            printf("%s, Error %d, %s: %s\n",
                PGM_NAME,errnum,errmess,errparam) >FILERR;
        }
    }

    exit(1);
}

function warning(errnum,errparam)
{
    if(MODE_VERBOSE == FALSE) return;

    errmess = errnum2mess(errnum);

    if(infile == TRUE) {
        if(errparam == "") {
            printf("%s, Warning %d reading file '%s' at line %d; %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess);
        }
        else {
            printf("%s, Warning %d reading file '%s' at line %d; %s: %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess,errparam);
        }
    }
    else {
        if(errparam == "") {
            printf("%s, Warning %d, %s\n",
                PGM_NAME,errnum,errmess);
        }
        else {
            printf("%s, Warning %d, %s: %s\n",
                PGM_NAME,errnum,errmess,errparam);
        }
    }
}

function errnum2mess(errnum)
{
    #--- Should not happen
    if     (errnum == 000) errmess = "SEE GIDE";

    #--- filini.k
    else if(errnum == 100) errmess = "ignoring line in .ini (NF!=2)";
    else if(errnum == 101) errmess = "invalid param-name in .ini";
    else if(errnum == 102) errmess = "unknown param-name in .ini";
    #--- filpar.k
    else if(errnum == 110) errmess = "ignoring line in .par (NF!=2)";
    else if(errnum == 111) errmess = "invalid param-name in .par";
    else if(errnum == 112) errmess = "invalid param-value in .par";
    else if(errnum == 113) errmess = "labJ: invalid nb of values";
    else if(errnum == 114) errmess = "nK: invalid nb of values";
    else if(errnum == 115) errmess = "unknown keyword in .par";
    #--- filphb.k
    else if(errnum == 120) errmess = "ignoring line in .phb (NF!=2)";
    else if(errnum == 121) errmess = "invalid param in .phb";
    else if(errnum == 122) errmess = "unknown keyword in .phb";
    else if(errnum == 123) errmess = "prohibition has no effect";
    else if(errnum == 127) errmess = "many products prohibited by";
    else if(errnum == 128) errmess = "nPATPHB = 0 ! (no prohibition)";
    else if(errnum == 129) errmess = "nPRDPHB = 0 ! (no prohibited products)";
    #--- fillgc.k
    else if(errnum == 130) errmess = "need to read .par before .lgc";
    else if(errnum == 131) errmess = ".lgc incompatible with .par";
    else if(errnum == 132) errmess = "unexpected NF in .lgc";
    #--- fildes.k
    else if(errnum == 140) errmess = "need to read .par before .des";
    else if(errnum == 141) errmess = ".des incompatible with .par";
    else if(errnum == 142) errmess = ".des incompatible with .par";
    else if(errnum == 143) errmess = "unexpected NF in .des";
    else if(errnum == 144) errmess = "unexpected NF in .des";
    #--- prd2dsc.k
    else if(errnum == 200) errmess = "internal: invalid product id (prd-string)";
    #--- prdtyp.k
    else if(errnum == 320) errmess = "pattern prohibited has no effect";
    #--- prdtyp.k
    else if(errnum == 400) errmess = "too many trials rejected, could not generate design !";

    #--- Par2Lgc.k
    else if(errnum == 500) errmess = "unknown argument in cmdline";
    else if(errnum == 501) errmess = "unexpected file";
    else if(errnum == 502) errmess = "nPRDVAL=0 !";
    #--- Par2Des.k
    else if(errnum == 600) errmess = "unknown argument in cmdline";
    else if(errnum == 601) errmess = "unexpected file";
    else if(errnum == 602) errmess = "nPRDVAL=0 !";
    #--- Des2Chk.k
    else if(errnum == 700) errmess = "unknown argument in cmdline";
    else if(errnum == 701) errmess = "unexpected file";
    else if(errnum == 702) errmess = "No .DES or .LGC file read"
    else if(errnum == 706) errmess = "many cells prohibited for";
    else if(errnum == 707) errmess = "many cells empty for";
    else if(errnum == 708) errmess = "one modality prohibited for";
    else if(errnum == 709) errmess = "one modality never used for";

    #--- Should not happen
    else                   errmess = "(undef)";

    return errmess;
}

# Function to avoid warnings "<var> set but not used"
function unused(arg)
{
    unused_ok = arg;
    return unused_ok;
}
