# makefile for Trade-Off, Design generation, Prohibitions handling & Checking
# 18 Avril 2014

# SYNTAX
# $ make -f $TRADEOFF/TOdesign.mak DOSSIER=<name>  clean
# $ make -f $TRADEOFF/TOdesign.mak DOSSIER=<name>  design
# $ make -f $TRADEOFF/TOdesign.mak DOSSIER=<name>  diag
#
# Notes: 
#   <name>.ini, <name>.par must exist
#   <name>.phb may exist


# GESTION SUFFIXES
# Clear out all suffixes
.SUFFIXES:
# List only those we use
.SUFFIXES: .wrn .chk .dev .des .lgv .lgc .phb .par .ini

# GOAL(S)
goal: design
design: $(DOSSIER).lgc $(DOSSIER).des $(DOSSIER).chk
diag: $(DOSSIER).lgv $(DOSSIER).dev $(DOSSIER).chk $(DOSSIER).wrn

# DEPENDENCIES
$(DOSSIER).lgv: $(DOSSIER).par $(DOSSIER).phb
$(DOSSIER).lgc: $(DOSSIER).par $(DOSSIER).phb
$(DOSSIER).dev: $(DOSSIER).ini $(DOSSIER).par $(DOSSIER).phb
$(DOSSIER).des: $(DOSSIER).ini $(DOSSIER).par $(DOSSIER).phb
$(DOSSIER).chk: $(DOSSIER).des $(DOSSIER).lgc

# IF NO $*.phb, THEN CREATE EMPTY $*.phb
$(DOSSIER).phb: $(DOSSIER).par
	touch $*.phb

# DEPENDENCIES & ACTIONS
.par.lgv:
	awk -f $(TRADEOFF)/Par2Lgc.awk verbose FS="=" $*.par FS="=" $*.phb  >$*.lgv
.par.lgc:
	awk -f $(TRADEOFF)/Par2Lgc.awk FS="=" $*.par FS="=" $*.phb  >$*.lgc

.par.dev:
	awk -f $(TRADEOFF)/Par2Des.awk verbose FS="=" $*.ini $*.par FS="=" $*.phb  >$*.dev
.par.des:
	awk -f $(TRADEOFF)/Par2Des.awk FS="=" $*.ini $*.par FS="=" $*.phb  >$*.des

.des.chk:
	awk -f $(TRADEOFF)/Des2Chk.awk verbose FS="=" $*.par FS=";" $*.des $*.lgc >$*.chk

.chk.wrn:
	echo "File .LGV - Logical constraints (verbose mode)"       >$*.wrn
	- grep "Error"   $*.lgv >>$*.wrn
	- grep "Warning" $*.lgv >>$*.wrn
	echo "File .DEV - Design (verbose mode)"      >>$*.wrn
	- grep "Error"   $*.dev >>$*.wrn
	- grep "Warning" $*.dev >>$*.wrn
	echo "File .CHK - Design+Constraints check (verbose mode)"      >>$*.wrn
	- grep "Error"   $*.chk >>$*.wrn
	- grep "Warning" $*.chk >>$*.wrn
	clear
	cat $*.wrn

clean:
	- rm $(DOSSIER).lgc $(DOSSIER).lgv $(DOSSIER).des $(DOSSIER).dev $(DOSSIER).chk
	- rm $(DOSSIER).wrn

