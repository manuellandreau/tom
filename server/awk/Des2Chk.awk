# TRADE-OFF - Program Des2Chk.awk, Design + Prohibitions checking
# Version 2.01, 10 May 2016
# J.-M. Bernard, GIDE (c)

# SYNTAX
# $ awk -f DesCheck.awk file.PAR file.DES [file.LGC] >file.CHK

# FUTUR / REVOIR
# * Possibilite de lire soit .DES, soit .LGC, soit les deux, fait a moitie
# * Numeros & messages d'erreur
# * Revoir choses faites & a faire pour "Repart. selon produits"
# * Peut lire deux types de .DES, avec/sans champ Prod (selon source int/ext)
#   mais aucun test n'est fait sur le nom des champs (l'ordre correct est 'assumed')
# * Si .LGC lu, distinguer PRD valid/invalid dans derniere liste


BEGIN {

    #===== PROGRAM
    PGM_NAME    = "Des2Chk.awk";
    PGM_VERSION = "2.01";
    PGM_DATE    = "10 May 2016";

    #===== CONSTANTES SYMBOLIQUES (FIXES)
    symbcst();
    #--- Select mode (JM ONLY)
    MODE_VERBOSE  = FALSE;
    #--- Source des informations analysees (.DES ou .LGC)
    SOURCE_DES = 1;
    SOURCE_LGC = 2;
    #--- Valeurs pour nPRD*
    nPRD_UNKNOWN   = -1;
    #--- Presentation sorties
#REVOIR: unused now ?
    NB_COLUMNS = 5;

    WARNCROISRATIO = 0.60;

    filerr = "/dev/stderr";


    #===== LECTURE COMMAND LINE (FILES & OPTIONS)
    for(c=1 ; c<ARGC ; c++) {
        filnam = ARGV[c];
        optnam = ARGV[c];
        if(filnam ~ FILPAR_PTN) {
            filpar = filnam;
            if(MODE_VERBOSE)
                printf("Command line: .PAR File '%s' (input)\n", filpar);
        }
        else if(filnam ~ FILDES_PTN) {
            fildes = filnam;
            if(MODE_VERBOSE)
                printf("Command line: .DES File '%s' (input)\n", fildes);
        }
        else if(filnam ~ FILLGC_PTN) {
            fillgc = filnam;
            if(MODE_VERBOSE)
                printf("Command line: .LGC File '%s' (input)\n", fillgc);
        }
        else if(optnam ~ "^verbose") {
            MODE_VERBOSE = TRUE;
            ARGV[c]="";
            #--- Output: Entete
            output_entete();
        }
        else if(optnam ~ "^FS=") {
        }
        else {
            error(700,ARGV[c]);
        }
    }
    if(MODE_VERBOSE) printf("\n");


    #===== INITIALISATIONS (nPRD calculated from .DES and/or .LGC, nPRDVAL from .LGC)
    nPRD    = 0;
    nPRDVAL = 0;
}

{
#REVOIR: peut-on eviter lecture fillgc si TOOMANY ? (mais calcul n'est fait que ds END{} !)
    #===== READING FILES .PAR, .DES & .LGC
    infile = TRUE;
    #--- Reading file .PAR
    if     (FILENAME == filpar) filpar_read();   
    #--- Reading file .DES
    else if(FILENAME == fildes) fildes_read();
    #--- Reading file .LGC
    else if(FILENAME == fillgc) fillgc_read();
    #--- What else ?
    else error(701,FILENAME);

}

END {
    infile = FALSE;

    #===== PRELIMINAIRES
    #--- MODE_VERBOSE
    if(MODE_VERBOSE) printf("\n");
    #--- Test fichiers attendus sont lus
    if(fildes_isread == FALSE && fillgc_isread == FALSE) 
        error(702);
    #--- Note: .des -> countPRD[], .lgc -> validPRD[]

#CHECK: recopie de Par2Lgc.awk sans veritable verification
    #===== PRODUITS: CALCUL NB PRODUITS APRIORI
    #--- Nb Produits a priori: nPRD
    nPRD = 1;
    for(j=1 ; j<=nJ ; j++)
        nPRD *= nK[j];

#CHECK: recopie de Par2Des.awk sans veritable verification
    #===== DEFINITION DE nPRD_NOTTOOBIG, cf. prdtyp et prdlab
    #--- Is number of PRD's too big ?
    nPRD_NOTTOOBIG = TRUE;
    if(nPRD >= PRDMANY)
        nPRD_NOTTOOBIG = FALSE;
    #--- Ignore .LGC file if too big
    if(nPRD_NOTTOOBIG == FALSE)
        fillgc_isread = FALSE; 
    #===== Info on nPRDVAL / nPRDPHB ?
    if(!fillgc_isread) {
        nPRDVAL = nPRD_UNKNOWN;
        nPRDPHB = nPRD_UNKNOWN;
    }
    else nPRDPHB = nPRD - nPRDVAL;

    #===== Contraintes sur Produits
    printf("Constraints/Prohibitions on products (from .LGC and/or .DES)\n");
    printf("  nPRD (a priori)    = %d\n", nPRD);
    printf("  nPRDVAL (valides)  = %d (%d if Unknown)\n", nPRDVAL,nPRD_UNKNOWN);
    printf("  nPRDPHB (prohibes) = %d (%d if Unknown)\n", nPRDPHB,nPRD_UNKNOWN);
    #---
    printf("Total Nb products necessary for TO (from .PAR)\n");
    nP = nI*nTbyI*nPbyT;
    printf("  nPRDused           = %d\n", nP);
    printf("\n");


    #===== Repartitions selon A,B,C,etc.
    printf("\n");
    printf("Counts by Attributes (from .LGC and/or .DES)\n\n");
    printf("Computing counts by attribute: ") >filerr;
    for(j1=1 ; j1<=nJ ; j1++) {
        printf(" %s", labJ[j1]) >filerr;
        if(fillgc_isread == TRUE) {
            if(nPRD_NOTTOOBIG) compos_atb(j1,SOURCE_LGC);
        }
        if(fildes_isread == TRUE) compos_atb(j1,SOURCE_DES);
        printf("\n");
    }
    printf("\n") >filerr;

    #===== Repartitions selon A*B,A*C,etc.
    printf("\n");
    printf("Counts by Crossing of attributes (from .LGC and/or .DES)\n\n");
    printf("Computing counts by crossings: ") >filerr;
    for(j1=1 ; j1<=nJ ; j1++) {
        for(j2=j1+1 ; j2<=nJ ; j2++) {
            printf(" %s*%s", labJ[j1],labJ[j2]) >filerr;
            if(fillgc_isread == TRUE) {
                if(nPRD_NOTTOOBIG) compos_crois(j1,j2,SOURCE_LGC);
            }
            if(fildes_isread == TRUE) compos_crois(j1,j2,SOURCE_DES);
            printf("\n");
        }
    }
    printf("\n") >filerr;

    #===== Repartitions selon PRD
    if(fildes_isread) {
        #--- Cas .des + .lgc infos: restriction aux produits valides
        if(fillgc_isread) {
            printf("\n");
            printf("Counts by Products (from .DES, valid products only)\n");
            prd = prdenum_first();
            for(prd in validPRD) {
                if(validPRD[prd]) {
                    printf("prd=%s nb=%5d\n", prd,countPRD[prd]);
                }
            }
        }   
        #--- Cas .des, sans .lgc infos: tous produits a priori 
        else {
            printf("\n");
            printf("Counts by Products (from .DES, all possible products)\n");
            prd = prdenum_first();
            for(prd in countPRD) {
                printf("prd=%s nb=%5d\n", prd,countPRD[prd]);
            } 
        }
 
        #--- Comptage/print nbs d'occurence de chaque prd
        printf("Balance of Counts?\n");
        countmax = 0;
        prd = prdenum_first();
        for(prd in countPRD) {
#REVOIR: ce test me semble bizarre/inutile
            if( !fillgc_isread || (fillgc_isread && validPRD[prd]) ) {
                count = countPRD[prd] + 0;
                tabcount[count] ++;
                if(count > countmax) countmax=count;
            }
        }

        #--- Affichage distribution des count's  
        for(count=0 ; count<=countmax ; count++) {
            if(count in tabcount)
                printf(" nb_occ=%5d nb_prd=%5d\n", count,tabcount[count]);
        }
        printf("\n");

    }


    #===== Rubbish
    unused(i); unused(t); unused(p); unused(nI); unused(nPbyT); unused(nTbyI);
    unused(TO_SYSTEM);
}


#=================================================
# COMPOSITION SELON 1 ATB, SELON CROIST DE 2 ATB's
#=================================================

function compos_atb(j,source)
{
    #--- Initialisation
    for(k=1 ; k<=nK[j] ; k++)
        countmod[k] = 0;

    #--- ??
    if(source == SOURCE_DES)
        printf("Products counts for %s (Nb products used in TO, cf .DES)\n",labJ[j]);
    if(source == SOURCE_LGC) 
        printf("Constraints on %s (Nb of valid products, cf. .LGC)\n",labJ[j]);

    #--- Cas SOURCE_DES (use info from countPRD[])
    if(source == SOURCE_DES) {
    for(prd in countPRD) {
        prd2dsc(prd);
        k = dsccur[j];
        countmod[k] += countPRD[prd];
    }
    }

    #--- Cas SOURCE_LGC (use info from validPRD[])
    if(source == SOURCE_LGC) {
    for(prd in validPRD) {
        prd2dsc(prd);
        k = dsccur[j];
        countmod[k] += validPRD[prd];
    }
    }
        
    for(k=1 ; k<=nK[j] ; k++) {
        printf("%s=%d nb=%d\n", labJ[j],k,countmod[k]);
    }
    for(k=1 ; k<=nK[j] ; k++) {
        if(countmod[k] == 0) {
            if(source == SOURCE_DES) warning(709,labJ[j]);
            if(source == SOURCE_LGC) warning(708,labJ[j]);
        }
    }    
}

function compos_crois(j1,j2,source)
{
    #--- Compute countcrois[k1,k2]
    for(k1=1 ; k1<=nK[j1] ; k1++)
        for(k2=1 ; k2<=nK[j2] ; k2++)
            countcrois[k1,k2] = 0;

    #--- Titre selon SOURCE
    if(source == SOURCE_DES) 
        printf("Products counts for %s * %s (Nb products used in TO, cf .DES)\n",labJ[j1],labJ[j2]);
    if(source == SOURCE_LGC) 
        printf("Constraints on %s * %s (Nb of valid products, cf. .LGC)\n",labJ[j1],labJ[j2]);

    #--- Cas SOURCE_DES (use info from countPRD[])
    if(source == SOURCE_DES) {
    for(prd in countPRD) {
        prd2dsc(prd);
        k1 = dsccur[j1];
        k2 = dsccur[j2];
        countcrois[k1,k2] += countPRD[prd];
    }
    }

    #--- Cas SOURCE_LGC (use info from validPRD[])
    if(source == SOURCE_LGC) {
    for(prd in validPRD) {
        prd2dsc(prd);
        k1 = dsccur[j1];
        k2 = dsccur[j2];
        countcrois[k1,k2] += validPRD[prd];
    }
    }

    #--- Print results
    for(k1=1 ; k1<=nK[j1] ; k1++) {
        printf("%s=%d  ", labJ[j1],k1);
        for(k2=1 ; k2<=nK[j2] ; k2++)
            printf("%s=%d nb=%d  ", labJ[j2],k2,countcrois[k1,k2]);
        printf("\n");
    }

    #--- Print warning if many empty cells
    ncell = 0;
    for(k1=1 ; k1<=nK[j1] ; k1++) {
        for(k2=1 ; k2<=nK[j2] ; k2++)
            if(countcrois[k1,k2] != 0) ncell++;
    }
    rcell = ncell / (nK[j1]*nK[j2]);
    if(rcell < WARNCROISRATIO) {
        epar = sprintf("%s*%s",labJ[j1],labJ[j2])
        if(source == SOURCE_DES) warning(707,epar);
        if(source == SOURCE_LGC) warning(706,epar);
    }
}



#===============================
# CONSTANTES SYMBOLIQUES (FIXES)
# 10 May 2016
#===============================

function symbcst()
{
    #===== SYSTEME
    TO_SYSTEM  = "Linux";
    #--- Valeur pour PRDMANY selon system
    if(TO_SYSTEM == "Linux") PRDMANY = 50000;
    else                     PRDMANY =  1000;

    #===== Logical constants
    TRUE = 1;
    FALSE = 0;

    #===== FILENAME patterns & initialisation
    FILINI_PTN = ".*\\.[Ii][Nn][Ii]";
    FILPAR_PTN = ".*\\.[Pp][Aa][Rr]";
    FILDES_PTN = ".*\\.[Dd][Ee][Ss]";
    FILLGC_PTN = ".*\\.[Ll][Gg][Cc]";
    FILPHB_PTN = ".*\\.[Pp][Hh][Bb]";
    filini = "";
    filpar = ""; 
    fildes = ""; 
    fillgc = "";
    filphb = "";
    filini_isread = FALSE;
    filpar_isread = FALSE;
    fildes_isread = FALSE;
    fillgc_isread = FALSE;
    filphb_isread = FALSE;

    FILERR = "/dev/stderr";

    #===== LABELS COLONNES (dans .CSV)
    Ilab   = "IDScn";
    Tlab   = "Task";
    Plab   = "ProdNum";
    PRDlab = "Prod";
    VALlab = "Valid";

    #===== VARIOUS
    #--- Flag indicating that enumeration of prd's is finished
    PRDENUM_END = "-1";
}
#==================================
# INPUT .PAR: PARAMETERS OF PROBLEM
# 10 May 2016
#==================================

# REVOIR
# * nPbyT vs nPbyTI: both accepted, nPbyT in doc only
# - tests syntaxe par=parval correcte

function filpar_read()
{

    #--- Things to do once
    if(FNR == 1) {
        filpar_isread = 1;
        #--- Mode VERBOSE
        if(MODE_VERBOSE)
            printf("Reading .PAR file '%s' FS='%s'\n",FILENAME,FS);
    }

    #--- Read parameters: nJ, labJ[], nK[], nI, nTbyI, nPbyT
    param_read();
}

function param_read()
{
    #----- INPUT: PARAMETRES DU PROBLEME

    #--- Skip comments + blank lines
    if($0 ~ "^#") next;
    if(length($0) == 0) next;
    if(NF == 0) next;

    #--- Split line into: parnam = parval[]
    if(NF != 2) {
        warning(110,$0);
        next;
    }
    else {
        n1 = split($1,parnam," ");
        n2 = split($2,parval," ");
        if(n1 == 1) {
            if(MODE_VERBOSE) printf("  Reading param-pb '%s'\n", parnam[1]);
            $1 = parnam[1];
        }
        else {
            error(111,$1);
        }
    }

    #--- Nb Attributs, nJ
    if($1 == "nJ") {
        if(n2 == 1) 
            nJ = $2+0;
        else
            error(112,parval);
    }
    #--- Labels des attributs, labJ[1...nJ]
    else if($1 == "labJ") {
        if(n2 != nJ) error(113);
        for(j=1 ; j<=nJ ; j++)
            labJ[j] = parval[j];
    }
    #--- Nb modalites des attributs, nK[1...nJ]
    else if($1 == "nK") {
        if(n2 != nJ) error(114);
        for(j=1 ; j<=nJ ; j++)
            nK[j] = parval[j]+0;
    }
    #--- Nb Individus/Scenarios
    else if($1 == "nI") {
        nI = $2+0;
    }
    #--- Nb taches par Individu
    else if($1 == "nTbyI") {
        nTbyI = $2+0;
    }
    #--- Nb Produits par Tache
    else if($1 == "nPbyT" || $1 == "nPbyTI") {
        nPbyT = $2+0;
    }
    #--- Unknown parameter
    else {
        warning(115,$1);  
    }
}

function param_par_output()
{
    printf("Parameters-Problem (input from .PAR)\n");

    printf("  nI = %d\n", nI);
    printf("  nJ = %d\n", nJ);
    printf("  labJ = [");
    for(j=1 ; j<=nJ ; j++)
        printf("%s ",labJ[j]);
    printf("]\n");
    printf("  nK = [");
    for(j=1 ; j<=nJ ; j++)
        printf("%d ",nK[j]);
    printf("]\n");
    printf("  nTbyI = %d\n", nTbyI);
    printf("  nPbyT = %d\n", nPbyT);

    printf("Parameters-Problem (computed)\n");

    nPbyI = nPbyT*nTbyI;
    printf("  nPbyI = %d\n", nPbyI);
    printf("  nP = %.0lf (Nb products used in TO)\n", nPbyI*nI);
    printf("  nPRD = %.0lf (Nb products possible a priori)\n", nPRD);
    printf("\n");
}



#=====================
# FICHIER .DES (INPUT)
# 10 May 2016
#=====================

# REVOIR
# - dans cas .DES par GIDE, verif concordance prd<->dsc ? utile ?
# - la variable (calculee ici) nPRDused (ancient nPRD) est-elle utilisee apres return ?

function fildes_read()
{      
    #--- Things to do once
    if(FNR == 1) {
        if(filpar_isread == FALSE) 
            error(140);
        fildes_isread = 1; 
        fildes_nPRD = 0; 
        #--- Mode VERBOSE
        if(MODE_VERBOSE)
            printf("Reading .DES file '%s', FS='%s'\n",FILENAME,FS);
    }

    #--- Fields line: check expected fields for $1,$2,$3 (event. $4)
    if(FNR == 1) {
        if(NF == 4+nJ) {
            if($1 != Ilab || $2 != Tlab || $3 != Plab || $4 != PRDlab)
                error(141);
        }
        else if(NF == 3+nJ) {
            if($1 != Ilab || $2 != Tlab || $3 != Plab)
                error(142);
        }
        else {
            error(143);
        }
        next;
    }
    #--- Data lines: case IDScn;Task;ProdNum;A;B;C... (.DES created elsewhere)
    else if(NF == 3+nJ) {
        i   = $1;
        t   = $2;
        p   = $3;
        for(j=1 ; j<=nJ ; j++)
            dsc[j] = $(j+3);
        prd = dsc2prd(dsc);

        countPRD[prd] ++;
        if(countPRD[prd] > 1) fildes_nPRD++;
    }
    #--- Data lines: case IDScn;Task;ProdNum;Prod;A;B;C... (.DES created by GIDE)
    else if(NF == 4+nJ) {
        i   = $1;
        t   = $2;
        p   = $3;
        prd = $4;

        countPRD[prd] ++;
        if(countPRD[prd] > 1) fildes_nPRD++;
    }
    else {
        error(144);
    }
}


#=====================
# FICHIER .LGC (INPUT)
# 10 May 2016
#=====================

function fillgc_read()
{      
    #--- Things to do once
    if(FNR == 1) {
        if(filpar_isread == FALSE) 
            error(130);
        fillgc_isread = 1; 
        fillgc_isvalid = 1; 
        #--- Mode VERBOSE
        if(MODE_VERBOSE)
            printf("Reading .LGC file '%s', FS='%s'\n",FILENAME,FS);
        prdnum_inlgc = 0;
    }

    #--- Check if .lgc available or not
    if(FNR == 1 && $0 ~ "^NotAvailable") {
        fillgc_isvalid = 0;
        fillgc_isread = 0;
        printf("  Information not available\n");
    }
    #--- Fields line: ignored
    else if(FNR == 1) {
        if(NF != 1+nJ+1) error(131);
        next;
    }
    #--- Data lines
    else if(NF == 1+nJ+1) {
        prd = $1;
        prdnum_inlgc ++;
        valid = $(nJ+2);
        validPRD[prd] = valid;
        if(prdnum_inlgc > nPRD) nPRD = prdnum_inlgc;
        if(valid)               nPRDVAL ++;
    }
    else {
        error(132);
    }
}

#================================================
# CONVERSION prd <-> dsc[] & ENUMERATION OF prd's
# 10 May 2016
#================================================

# GLOBALS: 
#   dsccur[], prd2dsc_isinit, prd2dsc_div[]
# FUNCTIONS
#   dsc2prd(dsc, )
#   prd2dsc(prd, )
#   prdenum_first()
#   prdenum_next()

# NOTES
# - These are new versions of functions prd2dsc() & dsc2prd():
#   for dealing with BIG TO, prd is now a string, not an int/float


#=========================
# CONVERSION prd <-> dsc[]
#=========================

function dsc2prd(dsc, j,prdstr,labj)
{
    PRD2DSC_SEP = "-";
    prdstr = sprintf("%d",dsc[1]);
    for(j=2 ; j<=nJ ; j++) {
        labj = sprintf("%s%d",PRD2DSC_SEP,dsc[j]);
        prdstr = prdstr labj;
    }
    return prdstr;
}

function prd2dsc(prdstr, j,nJ2)
{
#FUTUR: si pb de vitesse, dsc -> dsccur directement ?
    PRD2DSC_SEP = "-";
    nJ2 = split(prdstr,dsc,PRD2DSC_SEP);
    if(nJ2 != nJ) {
#REVOIR: message erreur avec parametres, non en mode DEBUG
        if(DEBUG) printf("Pb with prd='%s' nJ2=%d nJ=%d\n", prdstr,nJ2,nJ);
        error(200);
    }

    for(j=1 ; j<=nJ ; j++)
        dsccur[j] = dsc[j];

    #--- "Return" dsccur[]
}


#===============================
# ENUMERATION OF prd's
# Loop over prd's is done by:
#
#   prd = prdenum_first();
#   do {
#     ...
#     prd = prdenum_next();
#   } while(prd != PRDENUM_END);
#
#===============================

function prdenum_first(  j,prd)
{
    for(j=1 ; j<=nJ ; j++) 
        dsccur[j] = 1;
    prd = dsc2prd(dsccur);
    return prd;
}

function prdenum_next(  j,j1,prd)
{
    if(dsccur[nJ] < nK[nJ]) {
        dsccur[nJ]++;
        prd = dsc2prd(dsccur);
        return prd;
    }
    else {
        for(j=nJ; j>=1 ; j--) {
            if(dsccur[j] != nK[j]) 
                break;
        }
        if(j>=1) {
            dsccur[j]++;
            for(j1=j+1 ; j1<=nJ ; j1++)
                dsccur[j1]=1;
            prd = dsc2prd(dsccur);
            return prd;
        }
        else return PRDENUM_END;
    }
}        


#=====================================
# OLD VERSIONS (NOT USED ANYMORE)
#   prd was considered as an int/float
#=====================================

function dsc2prd_v1(dsc, j,k,coef,prd)
{
    prd = 1;
    coef = 1;
    for(j=nJ ; j>=1 ; j--) {
        k = dsc[j];
        prd += (k-1)*coef;
        coef *= nK[j];
    }
    return prd;
}

function prd2dsc_init_v1()
{
    prd2dsc_isinit = FALSE;
}

function prd2dsc_v1(prd, j,reste)
{

    #--- Initialisation des diviseurs successifs prd2dsc_div[]
    if(prd2dsc_isinit_v1 == FALSE) {
        prd2dsc_div[nJ] = 1;
        for(j=nJ-1 ; j>=1 ; j--) {
            prd2dsc_div[j] = prd2dsc_div[j+1] * (nK[j+1]+0);
        }
        prd2dsc_isinit = TRUE;
    }

    #--- Formule avec divisions successives
    reste = prd-1;
    for(j=1 ; j<=nJ ; j++) {
        dsccur[j] = int(reste / prd2dsc_div[j]) + 1;
        reste = reste % prd2dsc_div[j];
    }

    #--- "Return" dsccur[]
}

#===============
# ENTETE
# 10 May 2016
#===============

function output_entete()
{
    if(output_entete_done == FALSE) {
        printf("TRADE-OFF - %s\n",PGM_NAME);
        printf("Version %s, %s, GIDE\n\n",PGM_VERSION,PGM_DATE);
        output_entete_done = TRUE;
    }
}
#================
# ERRORS
#================

# REVOIR
# - distinguer warning's de info's ?

# Globals
# Functions
#   error()
#   warning()
#   errnum2mess()
#   unused()

function error(errnum,errparam)
{
    errmess = errnum2mess(errnum);

    if(infile == TRUE) {
        if(errparam == "") {
            printf("%s, Error %d reading file '%s' at line %d; %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess) >FILERR;
        }
        else {
            printf("%s, Error %d reading file '%s' at line %d; %s: %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess,errparam) >FILERR;
        }
    }
    else {
        if(errparam == "") {
            printf("%s, Error %d, %s\n",
                PGM_NAME,errnum,errmess) >FILERR;
        }
        else {
            printf("%s, Error %d, %s: %s\n",
                PGM_NAME,errnum,errmess,errparam) >FILERR;
        }
    }

    exit(1);
}

function warning(errnum,errparam)
{
    if(MODE_VERBOSE == FALSE) return;

    errmess = errnum2mess(errnum);

    if(infile == TRUE) {
        if(errparam == "") {
            printf("%s, Warning %d reading file '%s' at line %d; %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess);
        }
        else {
            printf("%s, Warning %d reading file '%s' at line %d; %s: %s\n",
                PGM_NAME,errnum,FILENAME,FNR,errmess,errparam);
        }
    }
    else {
        if(errparam == "") {
            printf("%s, Warning %d, %s\n",
                PGM_NAME,errnum,errmess);
        }
        else {
            printf("%s, Warning %d, %s: %s\n",
                PGM_NAME,errnum,errmess,errparam);
        }
    }
}

function errnum2mess(errnum)
{
    #--- Should not happen
    if     (errnum == 000) errmess = "SEE GIDE";

    #--- filini.k
    else if(errnum == 100) errmess = "ignoring line in .ini (NF!=2)";
    else if(errnum == 101) errmess = "invalid param-name in .ini";
    else if(errnum == 102) errmess = "unknown param-name in .ini";
    #--- filpar.k
    else if(errnum == 110) errmess = "ignoring line in .par (NF!=2)";
    else if(errnum == 111) errmess = "invalid param-name in .par";
    else if(errnum == 112) errmess = "invalid param-value in .par";
    else if(errnum == 113) errmess = "labJ: invalid nb of values";
    else if(errnum == 114) errmess = "nK: invalid nb of values";
    else if(errnum == 115) errmess = "unknown keyword in .par";
    #--- filphb.k
    else if(errnum == 120) errmess = "ignoring line in .phb (NF!=2)";
    else if(errnum == 121) errmess = "invalid param in .phb";
    else if(errnum == 122) errmess = "unknown keyword in .phb";
    else if(errnum == 123) errmess = "prohibition has no effect";
    else if(errnum == 127) errmess = "many products prohibited by";
    else if(errnum == 128) errmess = "nPATPHB = 0 ! (no prohibition)";
    else if(errnum == 129) errmess = "nPRDPHB = 0 ! (no prohibited products)";
    #--- fillgc.k
    else if(errnum == 130) errmess = "need to read .par before .lgc";
    else if(errnum == 131) errmess = ".lgc incompatible with .par";
    else if(errnum == 132) errmess = "unexpected NF in .lgc";
    #--- fildes.k
    else if(errnum == 140) errmess = "need to read .par before .des";
    else if(errnum == 141) errmess = ".des incompatible with .par";
    else if(errnum == 142) errmess = ".des incompatible with .par";
    else if(errnum == 143) errmess = "unexpected NF in .des";
    else if(errnum == 144) errmess = "unexpected NF in .des";
    #--- prd2dsc.k
    else if(errnum == 200) errmess = "internal: invalid product id (prd-string)";
    #--- prdtyp.k
    else if(errnum == 320) errmess = "pattern prohibited has no effect";
    #--- prdtyp.k
    else if(errnum == 400) errmess = "too many trials rejected, could not generate design !";

    #--- Par2Lgc.k
    else if(errnum == 500) errmess = "unknown argument in cmdline";
    else if(errnum == 501) errmess = "unexpected file";
    else if(errnum == 502) errmess = "nPRDVAL=0 !";
    #--- Par2Des.k
    else if(errnum == 600) errmess = "unknown argument in cmdline";
    else if(errnum == 601) errmess = "unexpected file";
    else if(errnum == 602) errmess = "nPRDVAL=0 !";
    #--- Des2Chk.k
    else if(errnum == 700) errmess = "unknown argument in cmdline";
    else if(errnum == 701) errmess = "unexpected file";
    else if(errnum == 702) errmess = "No .DES or .LGC file read"
    else if(errnum == 706) errmess = "many cells prohibited for";
    else if(errnum == 707) errmess = "many cells empty for";
    else if(errnum == 708) errmess = "one modality prohibited for";
    else if(errnum == 709) errmess = "one modality never used for";

    #--- Should not happen
    else                   errmess = "(undef)";

    return errmess;
}

# Function to avoid warnings "<var> set but not used"
function unused(arg)
{
    unused_ok = arg;
    return unused_ok;
}
