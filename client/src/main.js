import Vue from 'vue'
import Notifications from 'vue-notification'
import VueI18n from 'vue-i18n'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import messages from '../translations'

import store from './store'

Vue.config.productionTip = false

Vue.use(Notifications)
Vue.use(VueI18n)

import { BSpinner } from 'bootstrap-vue'
Vue.component('b-spinner', BSpinner)

const i18n = new VueI18n({
    locale: 'en',
    messages
})

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
