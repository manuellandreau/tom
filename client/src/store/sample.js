const sample = {
    namespaced: true,
    state: {
        task: 5,
        concept: 2,
        version: 100,
        none: false,
        sample: null
    },
    getters: {
        getTask: state => state.task,
        getConcept: state => state.concept,
        getVersion: state => state.version,
        isNone: state => state.none,
        getSample: state => state
    },
    mutations: {
        setTask: (state, payload) => (state.task = payload),
        setConcept: (state, payload) => (state.concept = payload),
        setVersion: (state, payload) => (state.version = payload),
        setNone: (state, payload) => (state.none = payload)
    }
}

export default sample
