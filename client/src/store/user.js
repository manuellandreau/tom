import axios from 'axios'
import store from '.'

export default {
    namespaced: true,
    state: {
        loading: false,
        user: null,
        alert: null
    },
    getters: {
        isLoading: state => state.loading,
        getUser: state => state.user,
        getTradoffs: state => (state.user ? [...state.user.tradoffs].reverse() : []),
        getAlert: state => state.alert
    },
    mutations: {
        setLoading: (state, payload) => (state.loading = payload),
        setUser: (state, payload) => (state.user = payload),
        setTradoffs: (state, payload) => state.user.tradoffs.push(payload),
        setAlert: (state, payload) => (state.alert = payload)
    },
    actions: {
        /**
         *
         * @param commit
         * @param dispatch
         * @returns {Promise}
         */
        fetchUser({ commit /*, dispatch*/ }) {
            return axios
                .get('/auth/me')
                .then(({ data }) => {
                    commit('setUser', data)
                })
                .catch(e => {
                    console.log(e)
                    // e.request && e.request.status && e.request.status === 401
                    //     ? dispatch('logout')
                    //     : commit('setAlert', { ...e, text: `Une erreur s'est produite` })
                })
        },
        /**
         *
         * @param dispatch
         * @returns {Promise}
         */
        saveTradoff({ dispatch, rootGetters }) {
            const tradoff = {
                name: 'tradoff_' + Date.now(),
                created: new Date(),
                variables: store.getters['variables/getVariables'],
                contraints: store.getters['contraints/isSkip'] ? null : store.getters['contraints/getQuery'],
                sample: store.getters['sample/getSample']
            }
            return axios.post('/users/tradoffs', tradoff).then(() => dispatch('fetchUser'))
        }
    }
}
