export default {
    namespaced: true,
    state: {
        variables: [
            {
                id: 1,
                label: 'Variable 1',
                name: 'VAR1',
                borneInf: 1,
                borneSup: 10,
                step: 1
            }
        ]
    },
    getters: {
        getVariables: state => state.variables
    },
    mutations: {
        setVariables: (state, payload) => (state.variables = payload),
        setVariable: (state, { index, variable }) =>
            (state.variables[index] = { ...state.variables[index], id: variable.currentId, ...variable }),
        addVariable: (state, payload) => state.variables.push(payload),
        removeVariable: (state, index) => state.variables.splice(index, 1)
    }
}
