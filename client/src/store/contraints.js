export default {
    namespaced: true,
    state: {
        query: {},
        preview: '',
        skip: false
    },
    getters: {
        getQuery: state => state.query,
        getPreview: state => state.preview,
        isSkip: state => state.skip
    },
    mutations: {
        setQuery: (state, payload) => (state.query = payload),
        setPreview: (state, payload) => (state.preview = payload),
        setSkip: (state, payload) => (state.skip = payload)
    }
}
