import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from './auth'
import user from './user'
import app from './app'
import variables from './variables'
import contraints from './contraints'
import sample from './sample'
import tradoffs from './tradoffs'

Vue.use(Vuex)

export default new Vuex.Store({
    strict: true,
    modules: {
        auth,
        user,
        app,
        variables,
        contraints,
        sample,
        tradoffs
    },
    plugins: [createPersistedState()]
})
