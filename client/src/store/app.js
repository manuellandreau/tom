const app = {
    namespaced: true,
    state: {
        lang: null,
        toggleLoginBox: false,
        step: 1,
        currentStep: 1
    },
    getters: {
        getLang: state => state.lang,
        getToggleLoginBox: state => state.toggleLoginBox,
        getStep: state => state.step,
        getCurrentStep: state => state.currentStep
    },
    mutations: {
        setLang: (state, payload) => (state.lang = payload),
        setToggleLoginBox: (state, payload) => (state.toggleLoginBox = payload),
        setStep: (state, payload) => (state.step = payload),
        setCurrentStep: (state, payload) =>
            (state.currentStep = state.currentStep < payload ? payload : state.currentStep)
    },
    actions: {
        switchLang({ commit /*, dispatch */ }, payload) {
            localStorage.setItem('lang', payload)
            return commit('setLang', payload)
        },
        reset({ commit }) {
            commit('setStep', 1)
            commit('setCurrentStep', 1)

            commit('variables/setVariables', [{ id: 1, label: 'Variable 1', name: 'VAR1' }], { root: true })

            commit('contraints/setQuery', null, { root: true })
            commit('contraints/setPreview', '', { root: true })

            commit('sample/setTask', 5, { root: true })
            commit('sample/setConcept', 2, { root: true })
            commit('sample/setVersion', 100, { root: true })
            commit('sample/setNone', false, { root: true })
        }
    }
}

export default app
