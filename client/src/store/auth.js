import axios from 'axios'

export default {
    namespaced: true,
    state: {
        authenticated: false,
        loading: false,
        alert: null
    },
    getters: {
        isAuthenticated: state => state.authenticated,
        isLoading: state => state.loading,
        alert: state => state.alert
    },
    mutations: {
        setAuthenticated: (state, payload) => (state.authenticated = payload),
        setLoading: (state, payload) => (state.loading = payload),
        setAlert: (state, payload) => (state.alert = payload)
    },
    actions: {
        /**
         * Log user from the jwt stored & Avoid logout at refresh
         * @param commit
         * @param dispatch
         */
        initAuthentication({ commit, dispatch }) {
            commit('setLoading', true)
            commit('setAlert', null)

            if (localStorage.getItem('token')) {
                commit('setAuthenticated', true)
                axios.defaults.headers.common.Authorization = localStorage.getItem('token')
                return dispatch('user/fetchUser', null, { root: true }).then(() => commit('setLoading', false))
            } else if (sessionStorage.getItem('token')) {
                commit('setAuthenticated', true)
                axios.defaults.headers.common.Authorization = sessionStorage.getItem('token')
                return dispatch('user/fetchUser', null, { root: true }).then(() => commit('setLoading', false))
            } else {
                commit('setAuthenticated', false)
                commit('setLoading', false)
            }

            return Promise.resolve()
        },
        /**
         * Login hanlder
         * @param commit
         * @param dispatch
         * @param {{identifier: string, password: string}} payload
         * @returns {Promise}
         */
        login({ commit, dispatch }, payload) {
            commit('setLoading', true)
            return axios
                .post('/auth/login', payload)
                .then(({ data }) => {
                    // noinspection JSUnresolvedVariable
                    axios.defaults.headers.common.Authorization = 'Bearer ' + data.access_token

                    // noinspection JSUnresolvedVariable
                    if (payload.remember) {
                        localStorage.setItem('token', 'Bearer ' + data.access_token)
                    } else {
                        sessionStorage.setItem('token', 'Bearer ' + data.access_token)
                    }

                    commit('setAlert', null)
                    commit('setAuthenticated', true)
                    commit('setLoading', false)

                    return dispatch('user/fetchUser', null, { root: true })
                })
                .catch(e => {
                    commit('setLoading', false)
                    commit(
                        'setAlert',
                        e.request && e.request.status && e.request.status === 401
                            ? { ...e, text: `Mauvais identifiant ou mot de passe` }
                            : { ...e, text: `Une erreur s'est produite` }
                    )
                    return Promise.reject()
                })
        },
        /**
         * Register hanlder
         * @param commit
         * @param dispatch
         * @param {{identifier: string, password: string}} payload
         * @returns {Promise}
         */
        register({ commit, dispatch }, payload) {
            commit('setLoading', true)
            return axios
                .post('/auth/register', payload)
                .then(({ data }) => {
                    // noinspection JSUnresolvedVariable
                    axios.defaults.headers.common.Authorization = 'Bearer ' + data.access_token

                    sessionStorage.setItem('token', 'Bearer ' + data.access_token)

                    commit('setAlert', null)
                    commit('setAuthenticated', true)
                    commit('setLoading', false)

                    return dispatch('user/fetchUser', null, { root: true })
                })
                .catch(e => {
                    commit('setLoading', false)
                    commit(
                        'setAlert',
                        e.request && e.request.status && e.request.status === 401
                            ? { ...e, text: `Un compte est déjà ascossié à cette adresse email` }
                            : { ...e, text: `Une erreur s'est produite` }
                    )
                    return Promise.reject()
                })
        },
        /**
         * Logout handler
         * @param commit
         * @returns {Promise<void>}
         */
        logout({ commit }) {
            commit('setAuthenticated', false)
            delete axios.defaults.headers.common.Authorization
            localStorage.removeItem('token')
            sessionStorage.removeItem('token')
            return Promise.resolve()
        },
        /**
         * Déclanche coté API l'envoi d'email de reinitialisation de MdP
         * @param commit
         * @param panelisteId
         * @returns {Promise}
         */
        sendResetMail({ commit }, panelisteId) {
            commit('setLoading', true)
            return axios
                .post('/auth/sendresetmail', panelisteId)
                .then(() => commit('setAlert', null))
                .catch(e => {
                    commit('setLoading', false)
                    commit(
                        'setAlert',
                        e.request && e.request.status && e.request.status === 400
                            ? { ...e, text: `Mauvais identifiant` }
                            : { ...e, text: `Une erreur s'est produite` }
                    )
                    return Promise.reject()
                })
        },
        /**
         * Suite à un reset/oubli de mot de passe
         * @param commit
         * @param payload
         * @returns {Promise}
         */
        renewPassword({ commit }, payload) {
            return axios.post('/auth/resetpw', payload).catch(e => {
                commit('setAlert', { ...e, text: `Une erreur s'est produite` })
                return Promise.reject()
            })
        },
        /**
         * Changement de mot de passe (page profil)
         * @param commit
         * @param payload
         * @returns {Promise}
         */
        changePassword({ commit }, payload) {
            return axios.post('/auth/changepw', payload).then(() =>
                commit('setAlert', {
                    type: 'succes',
                    text: 'Le nouveau mot de passe a été enregistré avec succès...'
                }).catch(e => {
                    commit('setAlert', { ...e, text: `Une erreur s'est produite` })
                    return Promise.reject()
                })
            )
        }
    }
}
