import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import store from './store'
import Home from './views/Home'

Vue.use(VueRouter)
axios.defaults.baseURL = process.env.VUE_APP_API_URL || 'http://localhost:8000/api'

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: () => import('./views/About.vue')
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/Login.vue')
        },
        {
            path: '/step-1-variables',
            name: 'step-1-variables',
            component: () => import('./views/Step1Variables.vue')
        },
        {
            path: '/step-2-contraints',
            name: 'step-2-contraints',
            component: () => import('./views/Step2Contraints.vue')
        },
        {
            path: '/step-3-sample',
            name: 'step-3-sample',
            component: () => import('./views/Step3Sample.vue')
        },
        {
            path: '/step-4-ending',
            name: 'step-4-ending',
            component: () => import('./views/Step4Ending.vue')
        },
        {
            path: '/*',
            name: '404',
            component: () => import('./views/404.vue')
        }
    ]
})

store.dispatch('auth/initAuthentication')

router.beforeEach((to, from, next) => {
    const authenticated = store.getters['auth/isAuthenticated']

    if (to.matched.some(route => route.meta.auth)) {
        if (!authenticated) next({ name: 'login' })
        else next({ name: to.meta.name })
    } else next({ name: to.meta.name })
})

export default router
