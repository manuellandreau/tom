export default {
    app: {
        login: {
            signin: 'Connectez vous !',
            signup: 'Créer un compte !'
        },
        header: {
            about: 'À propos',
            signin: 'Connexion',
            account: 'Mon compte',
            logout: 'Déconnexion'
        },
        stepper: {
            contraints: 'Contraintes',
            sample: 'Échantillon',
            download: 'Téléchargement'
        }
    },
    sideBar: {
        home: 'Acceuil',
        menuHead: 'Accès rapide',
        menu1: 'Générer',
        menu2: 'Calculer'
    },
    home: {
        title: 'Introduction fr',
        subTitle:
            'Lorem <b>ipsum</b> dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        menu1: "Je veux générer un plan d'expérience",
        menu2: "J'ai déjà mes données, je veux calculer les utilités/importances et jouer avec le simulateur",
        listTitle: ctx =>
            `Tradoff${!ctx.named('plural') ? '' : 's'} précédement généré${!ctx.named('plural') ? '' : 's'}`
    },
    about: {
        title: 'About fr',
        subTitle:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        content:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    step1Variables: {
        title: 'Introduction',
        subTitle:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        formTitle: 'Ajouter une variable',
        labelInput: {
            label: 'Libellé'
        },
        nameInput: {
            label: 'Nom'
        },
        optionAdvice: `
            Mes niveaux de prix ne sont pas réguliers, comment faire ? Nous vous suggérons
            d'utiliser le type de variable "options" si votre intervalle de nombre n'est pas
            linéaire. Ainsi, vous pourrez saisir vous-même vos différents prix !`,
        typeInput: {
            label: 'Sélectionnez le type de la variable',
            num: 'Numérique',
            opt: 'Options'
        },
        numInput: {
            inf: 'Borne inférieure *',
            sup: 'Borne supérieure *',
            step: 'Step *',
            intorfloat: '* Nombre entier ou à virgule'
        },
        optInput: {
            label: 'Ajoutez des options',
            placeholder: 'ajout tag'
        },
        preview: 'Prévisualisation : ',
        price: "S'agit-il d'un prix ?",
        fileImport: {
            title: 'Ou importer mon fichier',
            placeholder: 'Selectionner un fichier'
        },
        warning: 'Vous devez créer au moins une variable !',
        nextBtn: 'Suivant'
    },
    step2Contraints: {
        title: 'Introduction',
        subTitle:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        preview: 'Choose a valiable, an operator and set a value.',
        nextBtn: 'Suivant',
        AndOr: {
            addRow: 'Ajouter une ligne',
            addGroup: 'Ajouter un groupe'
        }
    },
    step3Sample: {
        title: 'Instructions',
        subTitle:
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        formTitle: 'Description de mon échantillon',
        taskLabel: 'Nombre de planches vues par répondant',
        taskAdvice: 'Attention vous êtes dans le rouge.',
        conceptLabel: 'Nombre de produits par planche',
        conceptAdvice: 'You should ...',
        noneLabel: 'Aucun',
        noneTooltip: 'I am tooltip <b>component</b> content!',
        versionLabel: 'Nombre de répondants',
        extraLeft: {
            title: 'Deserunt mollit',
            content:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        nextBtn: 'Générer le design'
    },
    step4Ending: {
        title: 'Design généré avec succès !',
        dlIn: 'Télécharger en',
        or: 'Ou',
        download: 'Télécharger',
        chartTitle: "Quelques statistiques sur l'équilibre et la robustesse du plan d'expérience généré",
        back: "Retour à l'accueil"
    },
    error404: {
        title: 'Page Not Found',
        message: 'It looks like you found a glitch in the matrix...',
        back: '&larr; Back to the previews page'
    }
}
